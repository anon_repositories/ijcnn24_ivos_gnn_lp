
# DAVIS2017 dataset paths

import os
import numpy as np
import h5py
import pickle
import cv2

import config as Config

DATASET_ID = 'davis2017'

PREPROCESSED_DATA_ROOT_FOLDER = os.path.join(Config.PATH_CACHED_DATA_FOLDER, 'preprocessed_data/')
CACHE_FOLDER = os.path.join(Config.PATH_CACHED_DATA_FOLDER, 'cache/')
GNN_LABEL_PRED_BASE_PATH = Config.PATH_REFINEMENT_DATASET_GEN_FOLDER   # set to None, if no need for refinement training

IM_FOLDER = os.path.join(Config.PATH_DAVIS_ROOT_FOLDER, 'DAVIS2017/DAVIS/JPEGImages/480p/')
GT_ANNOT_FOLDER = os.path.join(Config.PATH_DAVIS_ROOT_FOLDER, 'DAVIS2017/DAVIS/Annotations/480p/')
DATA_FOLDER_OPTFLOWS = os.path.join(PREPROCESSED_DATA_ROOT_FOLDER, 'optflow/')
DATA_FOLDER_GT_ANNOTS = os.path.join(PREPROCESSED_DATA_ROOT_FOLDER, 'annots/')
DATA_FOLDER_SEGMENTATION = os.path.join(PREPROCESSED_DATA_ROOT_FOLDER, 'segmentation/')

VIDEO_SETS = {
    'full (60+30)': {'train': ['bear', 'bmx-bumps', 'boat', 'boxing-fisheye', 'breakdance-flare', 'bus', 'car-turn',\
                                   'cat-girl', 'classic-car', 'color-run', 'crossing', 'dance-jump', 'dancing', 'disc-jockey',\
                                   'dog-agility', 'dog-gooses', 'dogs-scale', 'drift-turn', 'drone', 'elephant', 'flamingo',\
                                   'hike', 'hockey', 'horsejump-low', 'kid-football', 'kite-walk', 'koala', 'lady-running',\
                                   'lindy-hop', 'longboard', 'lucia', 'mallard-fly', 'mallard-water', 'miami-surf',\
                                   'motocross-bumps', 'motorbike', 'night-race', 'paragliding', 'planes-water', 'rallye',\
                                   'rhino', 'rollerblade', 'schoolgirls', 'scooter-board', 'scooter-gray', 'sheep',\
                                    'skate-park', 'snowboard', 'soccerball', 'stroller', 'stunt', 'surf', 'swing', 'tennis',\
                                    'tractor-sand', 'train', 'tuk-tuk', 'upside-down', 'varanus-cage', 'walking'],
                     'val': ['bike-packing', 'blackswan', 'bmx-trees', 'breakdance', 'camel', 'car-roundabout',\
                                 'car-shadow', 'cows', 'dance-twirl', 'dog', 'dogs-jump', 'drift-chicane', 'drift-straight',\
                                 'goat', 'gold-fish', 'horsejump-high', 'india', 'judo', 'kite-surf', 'lab-coat', 'libby',\
                                 'loading', 'mbike-trick', 'motocross-jump', 'paragliding-launch', 'parkour', 'pigs',\
                                 'scooter-black', 'shooting', 'soapbox']},
    'debug (3+3)': {'train': ['bear', 'bmx-bumps', 'boat'],\
                    'val': ['bike-packing', 'blackswan', 'bmx-trees']},
    'full val (0+30)': {'train': [],
                        'val': ['bike-packing', 'blackswan', 'bmx-trees', 'breakdance', 'camel', 'car-roundabout',\
                                 'car-shadow', 'cows', 'dance-twirl', 'dog', 'dogs-jump', 'drift-chicane', 'drift-straight',\
                                 'goat', 'gold-fish', 'horsejump-high', 'india', 'judo', 'kite-surf', 'lab-coat', 'libby',\
                                 'loading', 'mbike-trick', 'motocross-jump', 'paragliding-launch', 'parkour', 'pigs',\
                                 'scooter-black', 'shooting', 'soapbox']},
    'debug val (0+3)': {'train': [],\
                        'val': ['bike-packing', 'blackswan', 'bmx-trees']}
}

CUSTOM_IMSIZE_DICT = {'bike-packing': (480, 910), 'disc-jockey': (480, 1138), 'cat-girl': (480, 911), 'shooting': (480, 1152)}

def get_video_set_vidnames(video_set_id, split_id):
    '''
    Parameters:
        video_set_id, split_id: str
    Returns:
        list of str; vidnames in set & split
    '''
    assert split_id in ['train', 'val', 'all']
    if split_id == 'all':
        video_set = VIDEO_SETS[video_set_id]
        return list(set(video_set['train'] + video_set['val']))
    else:
        return VIDEO_SETS[video_set_id][split_id]

def get_true_annots(vidname):
    '''
    Parameters:
        vidname: str
    Returns:
        gt_annot: ndarray(n_ims, sy, sx) of uint8
    '''
    davis_annot_h5_path = os.path.join(DATA_FOLDER_GT_ANNOTS, 'annots_' + vidname + '.h5')
    h5f = h5py.File(davis_annot_h5_path, 'r')
    gt_annot = h5f['annots'][:].astype(np.uint8)
    h5f.close()
    if vidname == 'tennis':   # erroeneous label in 'tennis' sequence annotations: the tennis ball is labeled 255 instead of 3
        error_mask = gt_annot == 255
        assert np.any(error_mask)
        gt_annot[error_mask] = 3
    return gt_annot

def get_segmentation_data(vidname):
    '''
    Parameters:
        vidname: str
    Returns:
        seg_data_dict: dict{'lvl0_seg': ndarray(n_frames, sy, sx) of i32
                            'fvecs': ndarray(n_segs, n_ssn_features=18) of fl32
                            'fmaps_ds2': ndarray(n_frames, sy//2, sx//2, n_ssn_features=18) fl16}
    '''
    seg_h5_path = os.path.join(DATA_FOLDER_SEGMENTATION, 'ssn_' + str(vidname) + '.h5')
    h5f = h5py.File(seg_h5_path, 'r')
    seg_arr = h5f['lvl0_seg'][:]
    fvecs = h5f['fvecs'][:]
    fmaps_ds2 = h5f['fmaps_ds2'][:].astype(np.float16)
    h5f.close()
    return {'lvl0_seg': seg_arr, 'fvecs': fvecs, 'fmaps_ds2': fmaps_ds2}

def get_optflow_occlusion_data(vidname):
    '''
    Parameters:
        vidname: str
    Returns:
        flow_fw, flow_bw: ndarray(n_ims, sy, sx, 2:[dy, dx]) of fl16
        occl_fw, occl_bw: ndarray(n_ims, sy, sx) of bool_
    '''
    flows_h5_path = os.path.join(DATA_FOLDER_OPTFLOWS, 'optflow_videoflow_' + vidname + '.h5')
    h5f = h5py.File(flows_h5_path, 'r')
    flow_fw = h5f['flows'][:].astype(np.float16)
    flow_bw = h5f['inv_flows'][:].astype(np.float16)
    occl_fw = h5f['occls'][:].astype(np.bool_)
    occl_bw = h5f['inv_occls'][:].astype(np.bool_)
    h5f.close()
    return flow_fw, flow_bw, occl_fw, occl_bw

def get_img_data(vidname):
    '''
    Parameters:
        vidname: str
    Returns:
        ims_bgr: ndarray(n_frames, sy, sx, 3) of uint8
    '''
    vid_folder = os.path.join(IM_FOLDER, vidname)
    n_frames = len(os.listdir(vid_folder))
    ims_bgr = []
    for fr_idx in range(n_frames):
        im_path = os.path.join(vid_folder, str(fr_idx).zfill(5) + '.jpg')
        im = cv2.imread(im_path, cv2.IMREAD_COLOR)
        if im.shape != (480,854,3):
            assert vidname in CUSTOM_IMSIZE_DICT.keys()    # safety check
            im = cv2.resize(im, (854,480), interpolation=cv2.INTER_NEAREST)
        ims_bgr.append(im)
    ims_bgr = np.stack(ims_bgr, axis=0)    # (n_frames, sy, sx, 3) of ui8
    assert ims_bgr.dtype == np.uint8
    return ims_bgr

def get_refinement_training_data(vidname):
    '''
    Parameters:
        vidname: str
    Returns:
        annot_gnn_preds: list(n_sessions) of list(n_steps) of dict{
                                'curr_annot_fr_idx': int,
                                'new_scribbles': dict{lab - int: list(n_scribbles_with_lab) of ndarray(n_points_in_scribble, 2:[y,x]) of int32},
                                'davis_state_prev_preds': ndarray(n_seg, p_dim) fl32,
                                'davis_state_seed_prev': dict{fr_idx - int: seed_arr - ndarray(n_seeds, 3:[py, px, lab] of i32}
                                'davis_state_seed_prop_prev': dict{fr_idx - int: seed_arr - ndarray(n_seeds, 3:[py, px, lab] of i32}
                              };
    '''
    gnn_pred_folder_path = os.path.join(GNN_LABEL_PRED_BASE_PATH, vidname)
    gnn_pred_fnames = os.listdir(gnn_pred_folder_path)
    gnn_pred_fnames = [gnn_pred_fname for gnn_pred_fname in gnn_pred_fnames if gnn_pred_fname[-4:] == '.pkl']
    annot_gnn_preds = []
    for gnn_pred_fname in gnn_pred_fnames:
        gnn_pred_fpath = os.path.join(gnn_pred_folder_path, gnn_pred_fname)
        with open(gnn_pred_fpath, 'rb') as f:
            gnn_pred_data_item = pickle.load(f)
            annot_gnn_preds.append(gnn_pred_data_item)
    return annot_gnn_preds

