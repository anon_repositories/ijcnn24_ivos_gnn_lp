
# Configuration, paths, constant definitions.


# ---> For users: set the paths below

# TODO test base davisinteractive package for eval
PATH_DAVIS_INTERACTIVE_TRAINING = '/home/my_home/git/davis-interactive-gnn-annot-training/'   # our fork of the davisinteractive package that can be used for training
PATH_DAVIS_INTERACTIVE = PATH_DAVIS_INTERACTIVE_TRAINING    # the davisinteractive package used for evaluation; this can be either 
								   # 	- the original package (install from pip, see https://interactive.davischallenge.org)
								   #	- or our fork 
PATH_VIDEOFLOW_ROOT_FOLDER = '/home/my_home/git/VideoFlow/'
PATH_SSN_OPTFLOW_ROOT_FOLDER = '/home/my_home/git/ijcnn24_ivos_gnn_lp_ssn/'

PATH_DAVIS_ROOT_FOLDER = '/home/my_home/databases/DAVIS/'
PATH_CACHED_DATA_FOLDER = '/home/my_home/git/ijcnn24_ivos_gnn_lp/cached_data/'

PATH_CHECKPOINT_FOLDER = '/home/my_home/git/ijcnn24_ivos_gnn_lp/trained_models/'
PATH_REFINEMENT_DATASET_GEN_FOLDER = '/home/my_home/git/ijcnn24_ivos_gnn_lp/refinement_dataset/'
PATH_DAVIS_BENCHMARK_REPORT_FOLDER = '/home/my_home/git/ijcnn24_ivos_gnn_lp/davis_reports/'

# GNN training / eval

GNN_TRAINING_DAVIS_N_INTERACTIONS = 8
GNN_TRAINING_DAVIS_SESSION_FRAME_QUERY_POLICY = 'random_uniform'   #'random_uniform'   # None OR in ['random_uniform', 'random_linear_distance_prob']
GNN_N_PARALLEL_DAVIS_TR_SESSIONS = 20   # default: 20
GNN_N_PARALLEL_DAVIS_VAL_SESSIONS = 20  # default: 20

GNN_ENABLE_NODE_FEATURE_SESSION_STEP_IDX = True
GNN_ENABLE_NODE_FEATURE_N_CATEGORIES = True
GNN_HIDDEN_DIM = 15
GNN_N_LAYERS = 10
GNN_DROPOUT = .1
GNN_INFEAT_DROPOUT = .0
GNN_LABEL_DROPOUT = .0
GNN_READOUT = "mean"
GNN_GRAPH_NORM = True
GNN_BATCH_NORM = True
GNN_RESIDUAL = True
GNN_NORMALIZE_ADJ_ATTENTION_SCORES = False
GNN_MULTICLASS_LOSS = 'miou'   # in ['ce', 'miou', 'miou_fg_only', 'mse', 'lovasz']
GNN_WEIGHT_LOSS_BY_N_FG_CATEGORIES = True

GNN_INIT_LR = 5e-3
GNN_WEIGHT_DECAY = .0
GNN_LR_REDUCE_FACTOR = 0.6
GNN_LR_SCHEDULE_PATIENCE = 40
GNN_N_EPOCHS = 201
GNN_TR_EPOCH_SIZE = 32
GNN_VAL_EPOCH_SIZE = 16
GNN_N_EPOCHS_CHECKPOINT_FREQ = 200
GNN_MIN_LR = 1e-5

USE_SEEDPROP = True

# Dataset generation with GNN for refinement model training

REFINEMENT_DATASET_GEN_FRAME_QUERY_METHOD = 'random_linear_distance_prob'    # 'default', 'equidistant', 'choose_from_distant', 'random_uniform', 'random_linear_distance_prob'
REFINEMENT_DATASET_GEN_N_STEPS = 7

# Refinement model training / eval

REFINEMENT_MODEL_INIT_LR = 1e-4
REFINEMENT_MODEL_WEIGHT_DECAY = .0
REFINEMENT_MODEL_LR_REDUCE_FACTOR = 0.6
REFINEMENT_MODEL_LR_SCHEDULE_PATIENCE = 100
REFINEMENT_MODEL_MIN_LR = 1e-6
REFINEMENT_N_EPOCHS = 251
REFINEMENT_MODEL_TR_EPOCH_SIZE = 32
REFINEMENT_STEP_IDXS_TRAINING = [6]        # list of ints or None to use all steps in training

REFINEMENT_N_EPOCHS_CHECKPOINT_FREQ = 250
REFINEMENT_AUGMENT = True
REFINEMENT_LOSS_NAME = 'miou'        # 'ce', 'miou'
REFINEMENT_P_PRED_POWER_K = 4.        # None to disable, otherwise float
REFINEMENT_N_TRAINING_ITER_PER_SCALE = [15]   # [..., 4x, 2x, 1x]  # e.g., [3,3,3,4] or [15]
REFINEMENT_N_EVAL_ITER_PER_SCALE = [15]   # [..., 4x, 2x, 1x]      # e.g., [3,3,3,4] or [15]
REFINEMENT_CNN_N_CONV_CHANNELS_MULT = 16
REFINEMENT_CNN_N_SCALES = 4
REFINEMENT_BATCH_NORM = False
REFINEMENT_DROPOUT_RATE = 0.
REFINEMENT_N_CONV_LAYERS_PER_BLOCK = 3
REFINEMENT_TWO_CHANNEL_AFFINITY = True         # only estimating two directions instead of four; the other two is given as 1-aff_oposite
