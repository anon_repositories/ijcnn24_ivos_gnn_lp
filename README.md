# ijcnn24_ivos_gnn_lp

Anonymous repository for the main software component proposed in "Solving interactive video object segmentation with label-propagating neural networks", a paper under review for IJCNN 2024.

## Requirements
- PyTorch >= 1.4 (Tested with 1.12)
- NumPy
- scikit-image
- PIL
- h5py
- opencv (Tested with 4.5.5)
- VideoFlow https://github.com/XiaoyuShi97/VideoFlow
- Additional, custom dependencies (anonymous repositories):
    - https://gitlab.com/anon_repositories/ijcnn24_ivos_gnn_lp_davisinteractive.git
    - https://gitlab.com/anon_repositories/ijcnn24_ivos_gnn_lp_ssn.git

## Evaluating trained models on the DAVIS 2017 Interactive benchmark

Trained models (trained on the DAVIS 2017 training set only) can be found in the `./trained_models` folder. First, set the correct paths to the datasets and dependencies in `config.py`. Make sure that the paths for these trained models are correct in `run_davis_interactive_eval_with_refine.py`: check the `GNN_CHECKPOINT_FPATH` and `REFINEMENT_CHECKPOINT_FPATH` constants.

Then, run `./preprocessing/create_all_data.py` to run VideoFlow and SSN segmentation on the DAVIS dataset in advance.

Finally, run the following command:

```
python run_davis_interactive_eval_with_refine.py
```

to evaluate the trained models on the DAVIS 2017 Interactive benchmark (validation set). The mean J metrics for the individual sequences are printed out and the final results given by the benchmark are saved to the path given by `PATH_DAVIS_BENCHMARK_REPORT_FOLDER` in `config.py`. For the training of the models a custom, modified version of the `davisinteractive` package is used (see "Requirements" above) which can be used to run the benchmark as well. However, for the evaluation, the original `davisinteractive` package can be used too (https://github.com/albertomontesg/davis-interactive.git).

## Training models

Trained models (trained on the DAVIS 2017 training set only) can be found in the `./trained_models` folder.

The complete training procedure consists of three phases. First, a Graph Neural Network (GNN) model is trained on the DAVIS 2017 training set. Then, this model is used to generate a training set for the training of the pixel-level refinement model using data from the DAVIS 2017 training set and the predictions of the GNN model. Finally, the pixel-level refinement model is trained on the aforementioned dataset.

First, set the correct paths to the datasets and dependencies in `config.py`. Then, run `./preprocessing/create_all_data.py` to run VideoFlow and SSN segmentation on the DAVIS dataset in advance. Make sure that these are generated for the whole DAVIS training set: check which split is specified in `./preprocessing/create_all_data.py`.

Train the GNN model by executing:

```
python run_train_gnn.py
```

Then, the trained GNN model is used to generate the training set for the training of the refinement model. Set the path for the newly trained GNN model by assigning it to the constant `GNN_CHECKPOINT_FPATH` in `run_collect_refinement_dataset.py`. Following that, run this script by executing:

```
python run_collect_refinement_dataset.py
```

Finally, execute:

```
python run_train_refinement_model.py
```

to train a refinement model on the newly created dataset.
