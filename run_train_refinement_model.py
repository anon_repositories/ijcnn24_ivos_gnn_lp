
# Runner script to train pixel-level label refinement model

import config as Config
import sys
sys.path.append(Config.PATH_DAVIS_INTERACTIVE_TRAINING)

import os
import time

import random
import numpy as np
import torch
import torch.optim as Toptim
import torch.nn.functional as tnnF
from contextlib import ExitStack

from cache_manager import CacheManager
from refinement.refinement_model import RefinementModel
from refinement.refinement_data_generator_async import RefinementDataGenerator_Async
import util.util as Util

def run_model_one_epoch(model, device, data_gen, ep_size, optimizer, is_training):
    '''
    Parameters:
        model: RefinementModel instance
        device: str; target device (e.g., 'cuda')
        data_gen: object implementing __iter__ (e.g. RefinementDataGenerator instance)
        ep_size: int
        optimizer: None OR torch Optimizer instance; must not be None if training
        is_training: bool; can be used for validation loss/acc computation when 'is_training' is False
    Returns:
        epoch_loss: float
        optimizer: torch Optimizer instance
    '''
    if is_training:
        assert optimizer is not None
        model.train()
    else:
        model.eval()
    epoch_loss = 0.
    n_batches = 0

    # if not training enter "with torch.no_grad()" block
    train_it = data_gen.__iter__()
    with ExitStack() as context_stack:
        if not is_training:
            context_stack.enter_context(torch.no_grad())

        for sample_offset in range(0, ep_size, 1):
            x_gpu, x_ds2_gpu, y_in_gpu, y_true_gpu, _ = next(train_it)

            if is_training:
                optimizer.zero_grad()

            y_pred_gpu, _ = model.forward(x_gpu, y_in_gpu, x_ds2_gpu)   # T(sy, sx, n_cat) of fl32 PROBABILITIES (sum=1 along last dim)

            loss = model.loss(y_pred_gpu, y_true_gpu)
            if is_training:
                loss.backward()
                optimizer.step()

            epoch_loss += loss.detach().item()
            n_batches += 1
        epoch_loss /= n_batches

    return epoch_loss, optimizer


def predict_random_samples(model, device, data_gen, n_samples):
    '''
    Parameters:
        model: RefinementModel instance
        device: str; target device (e.g., 'cuda')
        data_gen: object implementing __iter__ (e.g. RefinementDataGenerator instance)
        n_samples: int
    Returns:
        ims_bgr_npy: list(n_samples) of ndarray(sy, sx, 3:[BGR]) of uint8
        ys_in_npy: list(n_samples) of ndarray(sy, sx, n_categories) of float32 PROBABILITIES
        ys_pred_npy: list(n_samples) of ndarray(sy, sx, n_categories) of float32 PROBABILITIES
        ys_true_npy: list(n_samples) of ndarray(sy, sx, n_categories) of float32 ONE-HOT
            (!!! sy, sx may vary)
        aff_maps_npy: list(n_samples) of ndarray(4, sy, sx) of float32 VALUES IN [0,1];
    '''
    assert n_samples >= 1
    model.eval()
    with torch.no_grad():
        ims_bgr_npy, ys_in_npy, ys_pred_npy, ys_true_npy, aff_maps_npy = [], [], [], [], []
        for sample_idx in range(n_samples):
            x_gpu, x_ds2_gpu, y_in_gpu, y_true_gpu, im_bgr_npy = next(data_gen)   # step idx is specified through RefConfig.STEP_IDXS_TRAINING

            ims_bgr_npy.append(im_bgr_npy)
            y_pred_gpu, aff_maps_gpu = model.forward(x_gpu, y_in_gpu, x_ds2_gpu)   # T(sy, sx, n_cat) of fl32 PROBABILITIES (sum=1 along last dim), T(4, sy, sx) of fl32
            
            ys_in_npy.append(y_in_gpu.detach().cpu().numpy())
            ys_pred_npy.append(y_pred_gpu.detach().cpu().numpy())
            ys_true_npy.append(y_true_gpu.detach().cpu().numpy())
            aff_maps_npy.append(aff_maps_gpu.detach().cpu().numpy())

        return ims_bgr_npy, ys_in_npy, ys_pred_npy, ys_true_npy, aff_maps_npy

if __name__ == '__main__':

    #SET_NAME = 'debug (3+3)'
    SET_NAME = 'full (60+30)'

    print("CONFIG --->")
    print({var: Config.__dict__[var] for var in dir(Config) if not var.startswith("__")})
    print("<--- CONFIG")

    DEVICE = 'cuda'
    IMSIZE_YX = (480,854)
    ASYNC_TRAINING_DATAGEN = True
    N_DS2_IN_CHANNELS = 18

    N_VIDS_IN_MEMORY_LIMIT = 3
    cache_manager = CacheManager(n_vids_in_memory_limit=N_VIDS_IN_MEMORY_LIMIT, loading_protocol='cache_only')
    DatasetClass = CacheManager.get_dataset_class()

    train_vidnames = DatasetClass.get_video_set_vidnames(SET_NAME, 'train')
    cache_manager.ensure_cache_exists(train_vidnames)
    del cache_manager   # no need for it as the async data generators create their own cache managers

    tr_augmenter = None
    if Config.REFINEMENT_AUGMENT is True:
        from util.refinement_augmenters import BasicAugmenter
        tr_augmenter = BasicAugmenter()

    DATAGEN_PARALLELISM_DISABLED = True   # little time is gained, too much extra memory is used if parallel
    tr_iter = RefinementDataGenerator_Async(vidnames=train_vidnames, target_device=DEVICE, \
                        n_samples_per_chunk=2400, augmenter=tr_augmenter, no_parallelism=DATAGEN_PARALLELISM_DISABLED)

    n_in_channels = tr_iter.get_n_xfeatures()
    model = RefinementModel(device=DEVICE, loss_name=Config.REFINEMENT_LOSS_NAME, n_in_channels=n_in_channels, \
                                n_tr_iters_per_scale=Config.REFINEMENT_N_TRAINING_ITER_PER_SCALE, \
                                n_eval_iters_per_scale=Config.REFINEMENT_N_EVAL_ITER_PER_SCALE, \
                                n_ds2_in_channels=N_DS2_IN_CHANNELS, \
                                n_channels_mult=Config.REFINEMENT_CNN_N_CONV_CHANNELS_MULT, \
                                n_unet_scales=Config.REFINEMENT_CNN_N_SCALES, \
                                batch_norm=Config.REFINEMENT_BATCH_NORM, \
                                n_conv_layers_per_block=Config.REFINEMENT_N_CONV_LAYERS_PER_BLOCK, \
                                p_pred_power_k=Config.REFINEMENT_P_PRED_POWER_K, \
                                dropout_rate=Config.REFINEMENT_DROPOUT_RATE, \
                                two_channel_aff=Config.REFINEMENT_TWO_CHANNEL_AFFINITY)
    model = model.to(DEVICE)

    try:
        t0 = time.time()
        train_losses = []
        model.train()
        optimizer = Toptim.Adam(model.parameters(), lr=Config.REFINEMENT_MODEL_INIT_LR, weight_decay=Config.REFINEMENT_MODEL_WEIGHT_DECAY)
        scheduler = Toptim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=Config.REFINEMENT_MODEL_LR_REDUCE_FACTOR,
                                                             patience=Config.REFINEMENT_MODEL_LR_SCHEDULE_PATIENCE, verbose=True)
        for epoch_idx in range(Config.REFINEMENT_N_EPOCHS):
            
            time_start = time.time()
            model.set_iter_mode(is_training=True)
            epoch_train_loss, optimizer = run_model_one_epoch(model, DEVICE, tr_iter, \
                                ep_size=Config.REFINEMENT_MODEL_TR_EPOCH_SIZE, optimizer=optimizer, \
                                is_training=True)

            epoch_time = time.time() - time_start
            train_losses.append(epoch_train_loss)

            loss_text = f"tr_loss: {epoch_train_loss}"
            print(f"Epoch {epoch_idx+1} / {Config.REFINEMENT_N_EPOCHS} -- time (s): {epoch_time}, lr: {optimizer.param_groups[0]['lr']}, {loss_text}")

            # Saving checkpoint
            if (epoch_idx % Config.REFINEMENT_N_EPOCHS_CHECKPOINT_FREQ == 0) and (epoch_idx > 0):
                os.makedirs(Config.PATH_CHECKPOINT_FOLDER, exist_ok=True)
                ckpt_path = os.path.join(Config.PATH_CHECKPOINT_FOLDER, "refinement_model_ep" + str(epoch_idx) + ".pkl")
                torch.save(model.state_dict(), ckpt_path)
                print("-> Checkpoint saved to:", ckpt_path)

            scheduler.step(epoch_train_loss)
        
    except KeyboardInterrupt:
        print('-' * 89)
        print('Exiting from training early because of KeyboardInterrupt.')
        
    print(f"Training refinement model completed. Total time taken: {((time.time()-t0)/3600):.4f} hrs")

