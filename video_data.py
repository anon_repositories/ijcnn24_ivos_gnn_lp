
# Class to store all data related to a single video (for both the GNN and the refinement models).

import numpy as np
import cv2
import os

from segmentation import Segmentation
import util.featuregen as FeatureGen
import util.imutil as ImUtil

class VideoData:

    '''
    USAGE ->
        - init through constructor -> creates segmentation object & graph data
        - serialize class (within: _drop_data_for_serialization())
        - deserialize (load) class
        - add image-like fields to the loaded class based on its target usage (
                gnn training: load_image_data_for_gnn_only(), 
                refine training: load_image_data_for_refinement_training_only(), 
                gnn & refine inference: load_all_image_data_for_inference()
            )
            (image-like data that can be loaded quickly from preprocessing data are not serialized)

    Member fields:
        data_source: str; either 'cache' or 'init' based on whether 'self' object was
                     created by loading from cache or through __init__ from preprocessed data
                     (NOT SERIALIZED) 

        vidname: str
        seg: Segmentation
        data: dict{name - str: data_item - ?};
    '''

    def __init__(self, vidname, imgs_bgr, gt_annot, flow_fw, flow_bw, occl_fw, occl_bw, seg_data_dict):

        self.data_source = 'init'
        self.vidname = vidname
        flow_fw = flow_fw.astype(np.float32)   # float16 can easily overflow when computing flow directions
        flow_bw = flow_bw.astype(np.float32)
        self.seg = Segmentation(vidname, seg_data_dict)
        fvecs_ssn = seg_data_dict['fvecs']

        self._init_data(imgs_bgr=imgs_bgr, gt_annot=gt_annot, flow_fw=flow_fw, flow_bw=flow_bw, \
                                                occl_fw=occl_fw, occl_bw=occl_bw, fvecs_ssn=fvecs_ssn)

    def get_seg(self):
        return self.seg

    # CUSTOM SERIALIZATION

    def __getstate__(self):
        assert self.data_source == 'init', "Object loaded from cache cannot be serialized again"
        self._drop_data_for_serialization()
        d = self.__dict__.copy()
        del d['data_source']
        return d

    def __setstate__(self, d):
        self.__dict__ = d
        self.data_source = 'cache'
    #

    def add_data(self, name, data_item):
        assert name not in self.data.keys()
        self.data[name] = data_item

    def get_data(self, name):
        return self.data[name]

    def drop_data(self, name):
        del self.data[name]

    def drop_multiple_data(self, names):
        for name in names:
            del self.data[name]
            
    # private

    def _init_data(self, imgs_bgr, gt_annot, flow_fw, flow_bw, occl_fw, occl_bw, fvecs_ssn):
        '''
        Processes raw data to init VideoData object.
        Parameters:
            <see CacheManager._create_cache_file() for details>
        Modifies members:
            self.data
        '''
        self.data = {}

        # add images
        self.add_data('annot_im', gt_annot.astype(np.uint8, copy=False))
        self.add_data('bgr_im', imgs_bgr.astype(np.uint8, copy=False))
        lab_im_fl32 = np.stack([cv2.cvtColor(im, cv2.COLOR_BGR2LAB) for im in imgs_bgr], axis=0).astype(np.float32) / 255.

        # TODO fl32 -> fl16, fl32 -> bool seems useless in the optflow/occlusion image data loaders
        self._load_and_pad_optflow_image_data(flow_fw, flow_bw)
        self._load_and_pad_occlusion_image_data(occl_fw, occl_bw)
        self.add_data('fmap_seg_ssn', fvecs_ssn)

        self._update_seg_features(lab_im_fl32=lab_im_fl32, \
                                    flow_fw_padded=self.get_data('of_fw_im').astype(np.float32), \
                                    flow_bw_padded=self.get_data('of_bw_im').astype(np.float32), \
                                    occl_fw_padded=self.get_data('occl_fw_im').astype(np.float32), \
                                    occl_bw_padded=self.get_data('occl_bw_im').astype(np.float32))
        #
        
    def _update_seg_features(self, lab_im_fl32, flow_fw_padded, flow_bw_padded, occl_fw_padded, occl_bw_padded):
        '''
        Recomputes all segmentation-dependent features.
        Expects all pixelwise features to be already added to self.
        Modifies members:
            self.data
        '''
        imgs_bgr = self.get_data('bgr_im')
        assert lab_im_fl32.dtype == flow_fw_padded.dtype == flow_bw_padded.dtype == np.float32
        assert occl_fw_padded.dtype == occl_bw_padded.dtype == np.float32

        # get segmentation image
        seg_im = self.seg.get_seg_im(framewise_seg_ids=False)
        seg_fr_idxs = self.seg.get_fr_idxs_from_seg_ids(np.arange(self.seg.get_n_segs_total()))
        seg_sizes = self.seg.get_seg_sizes()
        seg_bboxes_tlbr = self.seg.get_seg_bboxes_tlbr()
        seg_masks_in_bboxes = ImUtil.get_masks_in_bboxes(seg_im=seg_im, seg_fr_idxs=seg_fr_idxs, \
                                                         seg_bboxes_tlbr=seg_bboxes_tlbr)  # list(n_segs) of 2D ndarrays
        seg_idxs_yx, seg_idxs_offsets = ImUtil.get_mask_pix_idxs_in_bboxes(seg_im=seg_im, seg_fr_idxs=seg_fr_idxs, \
                                                seg_bboxes_tlbr=seg_bboxes_tlbr, seg_masks_in_bboxes=seg_masks_in_bboxes)
    
        # create segment features
        border_segs = np.zeros((self.seg.get_n_segs_total()), dtype=np.float32)
        border_segs_true = FeatureGen.get_border_segments(seg_im=seg_im)
        border_segs[border_segs_true] = 1.
        self.add_data('border_seg', border_segs)
        self.add_data('annot_seg', FeatureGen.apply_reduce_im2seg(img=self.get_data('annot_im')[..., None], seg_im=seg_im, seg_ids=None, \
                                seg_fr_idxs=seg_fr_idxs, seg_bboxes_tlbr=seg_bboxes_tlbr, seg_masks_in_bboxes=seg_masks_in_bboxes, \
                                seg_idxs_yx=seg_idxs_yx, seg_offsets=seg_idxs_offsets, reduction_func=FeatureGen.reduce_most_frequent_item)[:,0])

        u_labels_im = np.unique(self.get_data('annot_im'))
        u_labels_seg = np.unique(self.get_data('annot_seg'))
        assert np.array_equal(u_labels_im, u_labels_seg), "A label category is not present in the rounded to segments labeling"
        assert np.array_equal(u_labels_im, np.arange(u_labels_im.shape[0])), "Labels not from 0 to n_labels-1"
        assert u_labels_im.shape[0] >= 2
        self.add_data('n_labels', u_labels_im.shape[0])

        meanstd_lab_seg = FeatureGen.apply_reduce_im2seg(img=lab_im_fl32, seg_im=seg_im, seg_ids=None, \
                                seg_fr_idxs=seg_fr_idxs, seg_bboxes_tlbr=seg_bboxes_tlbr, seg_masks_in_bboxes=seg_masks_in_bboxes, \
                                seg_idxs_yx=seg_idxs_yx, seg_offsets=seg_idxs_offsets, reduction_func=FeatureGen.reduce_meanstd_nocast)
        assert meanstd_lab_seg.shape[1:] == (6,)
        self.add_data('mean_lab_seg', meanstd_lab_seg[:,:3])
        self.add_data('std_lab_seg', meanstd_lab_seg[:,3:])

        meanstd_of_fw_seg = FeatureGen.apply_reduce_im2seg(img=flow_fw_padded, seg_im=seg_im, seg_ids=None, \
                                seg_fr_idxs=seg_fr_idxs, seg_bboxes_tlbr=seg_bboxes_tlbr, seg_masks_in_bboxes=seg_masks_in_bboxes, \
                                seg_idxs_yx=seg_idxs_yx, seg_offsets=seg_idxs_offsets, reduction_func=FeatureGen.reduce_meanstd_nocast)
        meanstd_of_bw_seg = FeatureGen.apply_reduce_im2seg(img=flow_bw_padded, seg_im=seg_im, seg_ids=None, \
                                seg_fr_idxs=seg_fr_idxs, seg_bboxes_tlbr=seg_bboxes_tlbr, seg_masks_in_bboxes=seg_masks_in_bboxes, \
                                seg_idxs_yx=seg_idxs_yx, seg_offsets=seg_idxs_offsets, reduction_func=FeatureGen.reduce_meanstd_nocast)
        assert meanstd_of_fw_seg.shape[1:] == (4,)
        assert meanstd_of_bw_seg.shape[1:] == (4,)
        self.add_data('mean_of_fw_seg', meanstd_of_fw_seg[:,:2])
        self.add_data('std_of_fw_seg', meanstd_of_fw_seg[:,2:])
        self.add_data('mean_of_bw_seg', meanstd_of_bw_seg[:,:2])
        self.add_data('std_of_bw_seg', meanstd_of_bw_seg[:,2:])
        self.add_data('mean_occl_fw_seg', FeatureGen.apply_reduce_im2seg(img=occl_fw_padded[..., None], seg_im=seg_im, seg_ids=None, \
                                seg_fr_idxs=seg_fr_idxs, seg_bboxes_tlbr=seg_bboxes_tlbr, seg_masks_in_bboxes=seg_masks_in_bboxes, \
                                seg_idxs_yx=seg_idxs_yx, seg_offsets=seg_idxs_offsets, reduction_func=FeatureGen.reduce_mean))
        self.add_data('mean_occl_bw_seg', FeatureGen.apply_reduce_im2seg(img=occl_bw_padded[..., None], seg_im=seg_im, seg_ids=None, \
                                seg_fr_idxs=seg_fr_idxs, seg_bboxes_tlbr=seg_bboxes_tlbr, seg_masks_in_bboxes=seg_masks_in_bboxes, \
                                seg_idxs_yx=seg_idxs_yx, seg_offsets=seg_idxs_offsets, reduction_func=FeatureGen.reduce_mean))        
        self.add_data('fmap_seg', None)   # legacy: disabled fmap_seg (from MobileNet)

        spatial_edges = FeatureGen.find_spatial_temporal_edges(seg_im=seg_im, edge_type='spatial')  # (n_edges, 2)
        self.add_data('spatial_edges', spatial_edges)

        flow_edges, flow_edge_fs = FeatureGen.find_flow_edges_and_gnn_features(seg_im=seg_im, seg_sizes=seg_sizes, \
                            flow_fwd=flow_fw_padded, flow_bwd=flow_bw_padded)  # (n_edges, 2), (n_edges, 2, 4)
        self.add_data('flow_edges', flow_edges)
        self.add_data('flow_edge_fs', flow_edge_fs)

        all_edges, all_edges_merger_indices = ImUtil.merge_edge_lists_only([spatial_edges, flow_edges], return_index=True)
        self.add_data('all_edges_merger_idxs', all_edges_merger_indices)

        mean_lab_diff_edgefs = FeatureGen.edgefs_segdist_pairwise_L2dist(seg_fs=self.get_data('mean_lab_seg'), edges=all_edges)
        self.add_data('mean_lab_diff_edgefs', mean_lab_diff_edgefs)
        mean_fmap_ssn_diff_edgefs = FeatureGen.edgefs_segdist_pairwise_L2dist(seg_fs=self.get_data('fmap_seg_ssn'), edges=all_edges)
        self.add_data('mean_fmap_ssn_diff_edgefs', mean_fmap_ssn_diff_edgefs)
        
        mean_of_fw_diff_edgefs = FeatureGen.edgefs_optflow_diff(seg_fs_flow=self.get_data('mean_of_fw_seg'), edges=all_edges)
        mean_of_bw_diff_edgefs = FeatureGen.edgefs_optflow_diff(seg_fs_flow=self.get_data('mean_of_bw_seg'), edges=all_edges)
        self.add_data('mean_of_fw_diff_edgefs', mean_of_fw_diff_edgefs)
        self.add_data('mean_of_bw_diff_edgefs', mean_of_bw_diff_edgefs)

    def _drop_data_for_serialization(self):
        '''
        Deletes all entries from self.data dictionary that are not kept for cache creation.
        Modifies members:
            self.data
        '''
        self.drop_data('annot_im')
        self.drop_data('bgr_im')
        self.drop_data('of_fw_im')
        self.drop_data('of_bw_im')
        self.drop_data('occl_fw_im')
        self.drop_data('occl_bw_im')

    def load_image_data_for_gnn_only(self, imgs_bgr, gt_annot, flow_fw, flow_bw, occl_fw, occl_bw):
        '''
        Loads all image features that are needed in order to perform GNN training or inference.
        Parameters:
            <see CacheManager._create_cache_file() for details>
        Modifies members:
            self.data
        '''
        self.add_data('annot_im', gt_annot.astype(np.uint8, copy=False))
        self.add_data('bgr_im', imgs_bgr.astype(np.uint8, copy=False))
        self._load_and_pad_optflow_image_data(flow_fw, flow_bw)
        self._load_and_pad_occlusion_image_data(occl_fw, occl_bw)
        #

    def load_image_data_for_refinement_training_only(self, imgs_bgr, gt_annot, flow_fw, flow_bw, seg_data_dict, gnn_preds):
        '''
        Loads all image features that are needed in order to perform refinement training.
        Parameters:
            <see CacheManager._create_cache_file() for details>
        Modifies members:
            self.data
        '''
        self.add_data('annot_im', gt_annot.astype(np.uint8, copy=False))
        self.add_data('bgr_im', imgs_bgr.astype(np.uint8, copy=False))
        self._load_and_pad_optflow_image_data(flow_fw, flow_bw)
        fmaps_ds2 = seg_data_dict['fmaps_ds2']
        self.add_data('ssn_fmaps_ds2', fmaps_ds2.astype(np.float16, copy=False))
        self.add_data('gnn_preds_refinement_training_data', gnn_preds)

    def load_all_image_data_for_inference(self, imgs_bgr, gt_annot, flow_fw, flow_bw, occl_fw, occl_bw, seg_data_dict):
        '''
        Loads all image features that are needed in order to perform both GNN and refinement INFERENCE.
        Modifies members:
            self.data
        '''
        self.add_data('annot_im', gt_annot.astype(np.uint8, copy=False))
        self.add_data('bgr_im', imgs_bgr.astype(np.uint8, copy=False))
        self._load_and_pad_optflow_image_data(flow_fw, flow_bw)
        self._load_and_pad_occlusion_image_data(occl_fw, occl_bw)
        fmaps_ds2 = seg_data_dict['fmaps_ds2']
        self.add_data('ssn_fmaps_ds2', fmaps_ds2.astype(np.float16, copy=False))

    # individual image data loaders

    def _load_and_pad_optflow_image_data(self, flow_fw, flow_bw):
        '''
        Modifies members:
            self.data
        '''
        flow_fw_padded = np.empty((flow_fw.shape[0] + 1,) + flow_fw.shape[1:], dtype=np.float32)
        flow_fw_padded[-1,:,:,:] = 0.
        flow_fw_padded[:-1,:,:,:] = flow_fw
        self.add_data('of_fw_im', flow_fw_padded.astype(np.float16))
        flow_bw_padded = np.empty((flow_bw.shape[0] + 1,) + flow_bw.shape[1:], dtype=np.float32)
        flow_bw_padded[0,:,:,:] = 0.
        flow_bw_padded[1:,:,:,:] = flow_bw
        self.add_data('of_bw_im', flow_bw_padded.astype(np.float16))

    def _load_and_pad_occlusion_image_data(self, occl_fw, occl_bw):
        '''
        Modifies members:
            self.data
        '''
        occl_fw_padded = np.empty((occl_fw.shape[0] + 1,) + occl_fw.shape[1:], dtype=np.float32)
        occl_fw_padded[-1,:,:] = 0.
        occl_fw_padded[:-1,:,:] = occl_fw
        self.add_data('occl_fw_im', occl_fw_padded.astype(np.bool_))
        occl_bw_padded = np.empty((occl_bw.shape[0] + 1,) + occl_bw.shape[1:], dtype=np.float32)
        occl_bw_padded[0,:,:] = 0.
        occl_bw_padded[1:,:,:] = occl_bw
        self.add_data('occl_bw_im', occl_bw_padded.astype(np.bool_))

