
# Class to manage caching (serialization of VideoData objects with precomputed data)

import os
import pickle
import time

import datasets.DAVIS17 as Dataset
from video_data import VideoData

class CacheManager():

    '''
    Member fields:
        n_vids_in_memory_limit: int
        videodata_objs: dict{vidname - str: tuple(reference_count - int: video_data - VideoData)}
        loading_protocol: str
        release_mode: str; if 'eager', objects in self.videodata_objs with zero reference counts are released right away;
                           if 'lazy', objects with zero reference counts are only released when 
                                                                    self.n_vids_in_memory_limit would be exceeded otherwise;
    '''

    DATASET_TO_LOAD = 'davis2017'  # 'davis2017'

    if DATASET_TO_LOAD == 'davis2017':
        import datasets.DAVIS17 as Dataset
    else:
        assert False, "Not implemented."

    @staticmethod
    def get_dataset_class():
        return CacheManager.Dataset

    # PUBLIC

    def __init__(self, n_vids_in_memory_limit, loading_protocol='cache_only', release_mode='eager'):
        '''
        Parameters:
            n_vids_in_memory_limit: int; number of videos allowed to be loaded into memory at the same time;
                                        loading and releasing of videos is controlled manually: 
                                            when this limit is exceeded an AssertionError is raised
            loading_protocol: str; in ['cache_only', 'gnn_traineval', 'refine_train', 'all_eval']
        '''
        assert n_vids_in_memory_limit >= 1
        assert loading_protocol in ['cache_only', 'gnn_traineval', 'refine_train', 'all_eval']
        assert release_mode in ['eager', 'lazy']
        self.n_vids_in_memory_limit = n_vids_in_memory_limit
        self.loading_protocol = loading_protocol
        self.release_mode = release_mode
        os.makedirs(CacheManager.Dataset.CACHE_FOLDER, exist_ok=True)
        self.videodata_objs = {}

    def set_loading_protocol(self, loading_protocol):
        assert loading_protocol in ['cache_only', 'gnn_traineval', 'refine_train', 'all_eval']
        self.loading_protocol = loading_protocol

    def set_release_mode(self, release_mode):
        assert release_mode in ['eager', 'lazy']
        self.release_mode = release_mode

    def create_videodata_obj_reference(self, vidname):
        '''
        Returns the VideoData object with the given name. 
            Loads it into memory if not already there and increases the reference counter by one.
        '''
        if vidname not in self.videodata_objs:
            # VideoData not present in memory, loading from cache
            if (self.release_mode == 'lazy') and (len(self.videodata_objs) >= self.n_vids_in_memory_limit):
                self.try_releasing_a_zero_ref_count_object()
            assert len(self.videodata_objs) < self.n_vids_in_memory_limit, "CacheManager: Exceeded limit in the number " +\
                                                            "of VideoData objects loaded into the memory. The manual " +\
                                                            "release of videos is required by calling release_videodata_obj()."

            fpath = os.path.join(CacheManager.Dataset.CACHE_FOLDER, CacheManager.Dataset.DATASET_ID + '_' + vidname + '.pkl')
            viddata = self._load_from_cache(vidname, fpath)

            # load additional image data that was not cached and add them to the VideoData objects
            imgs_bgr = CacheManager.Dataset.get_img_data(vidname)    # (n_fr, sy, sx, 3) of ui8
            gt_annot = CacheManager.Dataset.get_true_annots(vidname)    # (n_fr, sy, sx) of ui8
            flow_fw, flow_bw, occl_fw, occl_bw = CacheManager.Dataset.get_optflow_occlusion_data(vidname)  
                                    # (n_frs, sy, sx, 2:[dy, dx]) of fl16, ..., (n_frs, sy, sx) of bool, ...

            if self.loading_protocol == "gnn_traineval":
                viddata.load_image_data_for_gnn_only(imgs_bgr=imgs_bgr, gt_annot=gt_annot, flow_fw=flow_fw, flow_bw=flow_bw, \
                                                                                    occl_fw=occl_fw, occl_bw=occl_bw)
            elif self.loading_protocol == "refine_train":
                seg_data_dict = CacheManager.Dataset.get_segmentation_data(vidname)    # dict
                gnn_preds = CacheManager.Dataset.get_refinement_training_data(vidname)   # list of ... (see CacheManager.Dataset module for details)
                viddata.load_image_data_for_refinement_training_only(imgs_bgr=imgs_bgr, gt_annot=gt_annot, \
                                                flow_fw=flow_fw, flow_bw=flow_bw, seg_data_dict=seg_data_dict, gnn_preds=gnn_preds)
            elif self.loading_protocol == "all_eval":
                seg_data_dict = CacheManager.Dataset.get_segmentation_data(vidname)    # dict
                viddata.load_all_image_data_for_inference(imgs_bgr=imgs_bgr, gt_annot=gt_annot, flow_fw=flow_fw, flow_bw=flow_bw, \
                                                occl_fw=occl_fw, occl_bw=occl_bw, seg_data_dict=seg_data_dict)
            elif self.loading_protocol == 'cache_only':
                pass
            else:
                assert False

            # increase reference count to VideoData object
            self.videodata_objs[vidname] = (1, viddata)
        else:
            n_refs, viddata = self.videodata_objs[vidname]
            self.videodata_objs[vidname] = (n_refs+1, viddata)

        return viddata

    def release_videodata_obj_reference(self, vidname):
        '''
        Decreases the reference counter of the VideoData object with the given name by one.
            If the reference counter reaches zero, the VideoData object may be removed from the memory.
        '''
        n_refs, viddata = self.videodata_objs[vidname]
        assert n_refs >= 1
        n_refs -= 1
        self.videodata_objs[vidname] = (n_refs, viddata)
        if (n_refs <= 0) and (self.release_mode == 'eager'):
            del self.videodata_objs[vidname]

    def try_releasing_a_zero_ref_count_object(self):
        '''
        Removes a VideoData object with zero reference count from self.videodata_objs dict if there are any.
        '''
        vid_to_delete = None
        for vidname, (n_refs, viddata) in self.videodata_objs.items():
            if n_refs == 0:
                vid_to_delete = vidname
                break
        if vid_to_delete is not None:
            del self.videodata_objs[vid_to_delete]

    def ensure_cache_exists(self, vidnames):
        '''
        Checks whether cache files are available for all videos with name in 'vidnames'. Missing cache files are created.
        Parameters:
            vidnames: list of str
        '''
        vidnames_uncached = [vidname for vidname in vidnames if not os.path.isfile(\
                    os.path.join(CacheManager.Dataset.CACHE_FOLDER, CacheManager.Dataset.DATASET_ID + '_' + vidname + '.pkl'))]
        fpaths_uncached = [os.path.join(CacheManager.Dataset.CACHE_FOLDER, CacheManager.Dataset.DATASET_ID + '_' + vidname + '.pkl') \
                                for vidname in vidnames_uncached]

        # some videos are not cached: cache needs to be created
        if len(vidnames_uncached) > 0:
            print("CacheManager: " + str(len(vidnames_uncached)) + " videos were not found in the cache, creating now...")

            N_PARALLEL_JOBS = 8   # None to disable parallel cache creation
            if N_PARALLEL_JOBS is not None:

                from joblib import Parallel, delayed
                t0 = time.time()
                Parallel(n_jobs=N_PARALLEL_JOBS)(delayed(CacheManager._create_cache_file)(*params) for params in \
                                                                            zip(vidnames_uncached, fpaths_uncached))
                t1 = time.time()
                print("CacheManager: Done creating cache for " + str(len(vidnames_uncached)) + " videos. Total time taken: " \
                                                                        + str(round(t1-t0, 2)) + " seconds.")
            else:
                for vidname, fpath in zip(vidnames_uncached, fpaths_uncached):
                    t0 = time.time()
                    CacheManager._create_cache_file(vidname, fpath)
                    t1 = time.time()
                    print("CacheManager: Video '", vidname, "' cache saved. Time taken: " + str(round(t1-t0, 2)) + " seconds.")

    # PRIVATE

    def _load_from_cache(self, vidname, cache_fpath):
        '''
        Loads preprocessed data from cache.
        Returns:
            VideoData instance
        '''
        assert vidname in cache_fpath
        assert os.path.isfile(cache_fpath)
        assert vidname not in self.videodata_objs

        pkl_file = open(cache_fpath, 'rb')
        pkl_dict = pickle.load(pkl_file)
        pkl_file.close()
        pkl_videodata_obj = pkl_dict['videodata_obj']
        return pkl_videodata_obj

    @staticmethod
    def _create_cache_file(vidname, cache_fpath):
        '''
        Preprocecesses raw data and saves it to a cache file per video.
        '''
        assert vidname in cache_fpath
        assert not os.path.isfile(cache_fpath)
        print("CacheManager: Creating cache for video '", vidname, "'...", )

        imgs_bgr = CacheManager.Dataset.get_img_data(vidname)    # (n_fr, sy, sx, 3) of ui8
        gt_annot = CacheManager.Dataset.get_true_annots(vidname)    # (n_fr, sy, sx) of ui8
        flow_fw, flow_bw, occl_fw, occl_bw = CacheManager.Dataset.get_optflow_occlusion_data(vidname)  
                                # (n_frs, sy, sx, 2:[dy, dx]) of fl16, ..., (n_frs, sy, sx) of bool, ...
        seg_data_dict = CacheManager.Dataset.get_segmentation_data(vidname)    # dict

        videodata_obj = VideoData(vidname=vidname, imgs_bgr=imgs_bgr, gt_annot=gt_annot, flow_fw=flow_fw, flow_bw=flow_bw, \
                                  occl_fw=occl_fw, occl_bw=occl_bw, seg_data_dict=seg_data_dict)
        pkl_file = open(cache_fpath, 'wb')
        pkl_dict = {}
        pkl_dict['videodata_obj'] = videodata_obj
        pkl_data = pickle.dump(pkl_dict, pkl_file)
        pkl_file.close()
        print("CacheManager: Saved cache for video '", vidname, "'.", )

