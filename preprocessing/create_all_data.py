
#   Load/create and save data to .pkl/.h5 archives for efficient loading.

import sys
sys.path.append('..')

from datasets import DAVIS17
import os
import gc
import cv2
import numpy as np

import torch
import create_annots as CreateAnnots
import create_segmentation as CreateSeg
import create_optflow_videoflow as CreateOptFlowVideoFlow

def load_imgs_for_video(base_folder_path, vidname):
    '''
    Parameters:
        base_folder_path: str
        vidname: str
    Returns:
        ims: ndarray(n_frames, sy, sx, 3:BGR) of uint8
    '''
    vid_folder = os.path.join(base_folder_path, vidname)
    n_frames = len(os.listdir(vid_folder))
    ims = []
    for fr_idx in range(n_frames):
        im_path = os.path.join(vid_folder, str(fr_idx).zfill(5) + '.jpg')
        im = cv2.imread(im_path, cv2.IMREAD_COLOR)
        if im.shape != (480,854,3):
            assert vidname in DAVIS17.CUSTOM_IMSIZE_DICT.keys()    # safety check
            im = cv2.resize(im, (854,480), interpolation=cv2.INTER_NEAREST)
        ims.append(im)
    ims = np.stack(ims, axis=0)    # (n_frames, sy, sx, 3) of ui8
    assert ims.dtype == np.uint8
    return ims

def run_pytorch(vidnames, imgs_folder, annot_folder, optflow_out_folder, seg_out_folder, annot_out_folder, device):
    '''
    Parameters:
        vidnames: list of str; e.g., ['bear', 'blackswan']
        imgs_folder: str; base folder of DAVIS .jpg images, e.g., '/home/my_home/databases/DAVIS2017/DAVIS/JPEGImages/480p/'
        annot_folder: str; base folder of DAVIS .png format GT annotations, e.g., '/home/my_home/databases/DAVIS2017/DAVIS/Annotations/480p/'
        optflow_out_folder: str; output folder for the generated optical flow result archives
        seg_out_folder: str; output folder for the generated superpixel segmentation result archives
        annot_out_folder: str; output folder for the generated GT annotation archives
        device: str
    '''
    print("Fetching/generating cache to make model training more efficient...")

    # load image data
    print("Preprocessing: loading image data...")
    ims_dict = {vidname: load_imgs_for_video(imgs_folder, vidname) for vidname in vidnames}
    
    print("Preprocessing: running optical flow estimation...")
    of_fw_dict, of_bw_dict, _, _ = CreateOptFlowVideoFlow.run(ims_dict, optflow_out_folder, 'optflow_videoflow_', device)

    # create segmentation
    #   SSN with optical flow; to disable optical flow input (e.g. to use base SSN model, pass None values instead of the optflow dicts below)
    print("Preprocessing: creating segmentation...")
    CreateSeg.run(ims_dict, vidnames, seg_out_folder, optflow_fw_dict=of_fw_dict, optflow_bw_dict=of_bw_dict)  # TODO use 'device' here too

    # create & save annotations into .h5 archives
    print("Preprocessing: collecting annotations into a single file per video...")
    CreateAnnots.run(annot_folder, vidnames, annot_out_folder, 'annots_', 'annots')

    # deallocate PyTorch objects
    gc.collect()
    torch.cuda.empty_cache()

    print("Done preprocessing (PyTorch part).")


if __name__ == '__main__':

    DEVICE = 'cuda:0'
    #vidnames = DAVIS17.get_video_set_vidnames('debug (3+3)', 'all')
    vidnames = DAVIS17.get_video_set_vidnames('full val (0+30)', 'all')
    #vidnames = DAVIS17.get_video_set_vidnames('full (60+30)', 'all')
    #vidnames = ['night-race']

    N_VIDS_TO_PROCESS_AT_ONCE = 24   # to reduce memory consumption (approx. 40g if this var is set to 24)
    vidnames_splits = [vidnames[i:i+N_VIDS_TO_PROCESS_AT_ONCE] for i in range(0, len(vidnames), N_VIDS_TO_PROCESS_AT_ONCE)]
    for vidnames_split in vidnames_splits:
        run_pytorch(vidnames=vidnames_split,
            imgs_folder=DAVIS17.IM_FOLDER,
            annot_folder=DAVIS17.GT_ANNOT_FOLDER,
            optflow_out_folder=DAVIS17.DATA_FOLDER_OPTFLOWS,
            seg_out_folder=DAVIS17.DATA_FOLDER_SEGMENTATION,
            annot_out_folder=DAVIS17.DATA_FOLDER_GT_ANNOTS,
            device=DEVICE)
