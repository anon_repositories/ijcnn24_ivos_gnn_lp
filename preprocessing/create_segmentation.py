
#   Superpixel segmentation and feature map/vector estimation with the SSN superpixel method.
#   Original SSN superpixel segmentation:
#       Jampani et al.: Superpixel Sampling Networks
#   Extension of SSN to support training on both color and optical flow channels:
#       https://gitlab.com/anon_repositories/ijcnn24_ivos_gnn_lp_ssn.git

import sys
sys.path.append('..')

import numpy as np
import os
import h5py
import torch
import torch.nn.functional as tnnF

import config as Config

# optflow composite SSN model (from https://gitlab.com/anon_repositories/ijcnn24_ivos_gnn_lp_ssn.git)

SSN_MODEL_PATH = os.path.join(Config.PATH_SSN_OPTFLOW_ROOT_FOLDER, 'trained_models/best_composite_model.pth')
sys.path.append(Config.PATH_SSN_OPTFLOW_ROOT_FOLDER)
import inference_optflow_composite_model as SSNInferenceOptflow


class SSN_interface:

    '''
    Member fields:
        params_dict: dict{param_name - str: param_value - ?}
        model: SSNModel instance (torch.nn.Module subclass)
    '''

    def __init__(self, params_dict):
        self.params_dict = params_dict
        print("Initializing SSN optflow-composite type model...")
        self.model = SSNInferenceOptflow.init_optflow_composite_model(color_feature_dim=params_dict['SSN_COLOR_FDIM'], \
                                            optflow_feature_dim=params_dict['SSN_OPTFLOW_FDIM'], \
                                             n_iter=params_dict['SSN_N_ITER'], composite_weights_path=SSN_MODEL_PATH)

    def _save_seg_archive(self, seg_fpath, seg, spix_fvecs_all, pix_fmaps_all):
        '''
        Saves segmentation to a .h5 archive.
        Parameters:
            seg_fpath: str
            seg: ndarray(n_frames, sy, sx) of i32; video segmentation with IDs starting from 0
            spix_fvecs_all: ndarray(n_sp, n_ch_no_yx) of fl32; deep feature channels + LAB channels
            pix_fmaps_all: ndarray(n_frames, sy//2, sx//2, n_ch_no_yx) of fl16; deep feature channels + LAB channels
        '''
        assert seg.ndim == 3
        assert seg.shape[0] == pix_fmaps_all.shape[0]
        assert seg.shape[1]//2 == pix_fmaps_all.shape[1]
        assert seg.shape[2]//2 == pix_fmaps_all.shape[2]
        assert spix_fvecs_all.shape[1] == pix_fmaps_all.shape[3]
        h5f = h5py.File(seg_fpath, 'w')
        h5f.create_dataset('lvl0_seg', data=seg, dtype=np.int32, compression="gzip")
        h5f.create_dataset('fvecs', data=spix_fvecs_all, dtype=np.float32)
        h5f.create_dataset('fmaps_ds2', data=pix_fmaps_all, dtype=np.float16)   # gzip compression reduces size by 20% approx. (slower loading)
        h5f.close()

    def segment_video(self, vidname, ims, seg_out_folder, optflow_fw_ims=None, optflow_bw_ims=None):
        '''
        Creates segmentation over a video.
        Parameters:
            vidname: str
            ims: ndarray(n_frames, sy, sx, 3) of ui8
            seg_out_folder: str; output path for the .h5 packages, e.g., '/home/my_home/git/ijcnn24_ivos_gnn_lp/cached_data/preprocessed_data/segmentation/'
            (OPTIONAL) optflow_fw_ims, optflow_bw_ims: ndarray(n_frames-1, sy, sx, 2:[dy, dx]) of fl16
        '''
        n_frames = ims.shape[0]
        assert ims.shape[3:] == (3,)
        ims_rgb_fl32 = ims[..., ::-1].astype(np.float32)/255.  # bgr ui8  in [0,255] -> rgb fl32 in [0,1]

        if optflow_fw_ims is not None:
            assert optflow_fw_ims.shape == optflow_bw_ims.shape == (n_frames-1,) + ims.shape[1:3] + (2,)
            optflow_ims_fl32 = np.empty((n_frames,) + ims.shape[1:3] + (4,))
            optflow_ims_fl32[:-1,:,:,:2] = optflow_fw_ims.astype(np.float32)
            optflow_ims_fl32[1:,:,:,2:] = optflow_bw_ims.astype(np.float32)

        n_target_segs_per_fr = int(self.params_dict['N_K_TARGET_SEGMENTS_TOTAL']*1000 / n_frames)
        print("    Working on video '" + vidname + "', " + str(n_target_segs_per_fr) + " target segments per frame")
        
        n_segs = 0
        seg = np.full(ims_rgb_fl32.shape[:3], dtype=np.int32, fill_value=-1)
        spix_fvecs_all = []
        pix_fmaps_all = []
        for fr_idx in range(ims_rgb_fl32.shape[0]):

            seg_fr, spix_fvecs, pix_fmaps = SSNInferenceOptflow.inference_optflow_composite_model(comp_model=self.model, \
                                    image_rgb=ims_rgb_fl32[fr_idx], image_optflow=optflow_ims_fl32[fr_idx], \
                                    nspix=n_target_segs_per_fr, color_scale=self.params_dict['SSN_COLOR_SCALE'], 
                                    pos_scale=self.params_dict['SSN_POSITION_SCALE'], \
                                    optflow_scale=self.params_dict['SSN_OPTFLOW_SCALE'], \
                                    enforce_connectivity=False, return_feature_vecs=True, return_feature_map=True, \
                                    base_features_in_return=True, return_translation_invariant_features=True)
                                # ndarray(H, W), ndarray(B=1, C, n_SP), Tensor(B=1, C, H, W)
                                # segment connectivity is not enforced as it is not supported with the return_feature_vecs=True option

            seg_fr_end_offset = spix_fvecs.shape[2]
            u_labels = np.unique(seg_fr)
            if u_labels.shape[0] != seg_fr_end_offset:
                _, inv_labels = np.unique(seg_fr, return_inverse=True)
                seg_fr = inv_labels.reshape(seg_fr.shape)
                spix_fvecs = spix_fvecs[:,:,u_labels]
                # print("Warning! segment_video(): a segment has no pixels associated with it; deleted segment.")
                # print("    INFO:", u_labels.shape, np.amin(u_labels), np.amax(u_labels), seg_fr_end_offset)
                u_labels = np.unique(seg_fr)
                seg_fr_end_offset = u_labels.shape[0]
                assert seg_fr_end_offset == spix_fvecs.shape[2]

            seg[fr_idx,:,:] = seg_fr + n_segs
            n_segs += seg_fr_end_offset

            # downscale fmaps by 2x2
            pix_fmaps_ds_size_yx = (pix_fmaps.shape[2] // 2, pix_fmaps.shape[3] // 2)
            pix_fmaps = pix_fmaps[:,:-2,:,:]   # removing Y,X channels from the end (remaining: deep feature + L,A,B channels)
            pix_fmaps = tnnF.interpolate(pix_fmaps, size=pix_fmaps_ds_size_yx, mode='bilinear', align_corners=False)  # T(B=1, C, H//2, W//2)
            pix_fmaps = pix_fmaps.to(torch.float16).permute(0, 2, 3, 1)[0].to("cpu").detach().numpy()   # (H//2, W//2, C) fl16

            spix_fvecs = spix_fvecs[:,:-2,:]   # removing Y,X channels from the end (remaining: deep feature + L,A,B channels)
            spix_fvecs = spix_fvecs[0].transpose(1, 0)  # (n_SP, C)

            pix_fmaps_all.append(pix_fmaps)
            spix_fvecs_all.append(spix_fvecs)
            
        seg_fpath = os.path.join(seg_out_folder, 'ssn_' + str(vidname) + '.h5')
        pix_fmaps_all = np.stack(pix_fmaps_all, axis=0)
        spix_fvecs_all = np.concatenate(spix_fvecs_all, axis=0)
        self._save_seg_archive(seg_fpath, seg, spix_fvecs_all, pix_fmaps_all)
    #

def _run_for_single_video(vidname, ims, seg_out_folder, optflow_fw=None, optflow_bw=None):
    '''
    Runs the segmentation script for a single video. Method can be used in a parallel context.
    Parameters:
        vidname: str
        ims: ndarray(n_frames, sy, sx, 3) of ui8
        seg_out_folder: str; output path for the .h5 packages, e.g., '/home/my_home/git/ijcnn24_ivos_gnn_lp/cached_data/preprocessed_data/segmentation/'
        (OPTIONAL) optflow_fw, optflow_bw: ndarray(n_imgs-1, 480, 854, 2:[dy, dx]) of fl16
    '''
    params_dict = {}
    params_dict['N_K_TARGET_SEGMENTS_TOTAL'] = 60
    params_dict['SSN_COLOR_FDIM'] = 15
    params_dict['SSN_OPTFLOW_FDIM'] = 10
    params_dict['SSN_N_ITER'] = 5
    params_dict['SSN_COLOR_SCALE'] = 0.26
    params_dict['SSN_POSITION_SCALE'] = 4.0
    params_dict['SSN_OPTFLOW_SCALE'] = 5.0

    assert params_dict['N_K_TARGET_SEGMENTS_TOTAL'] < 100
    ssn_obj = SSN_interface(params_dict)   # TODO init once only in run(), not before each video
    ssn_obj.segment_video(vidname=vidname, ims=ims, seg_out_folder=seg_out_folder, \
                            optflow_fw_ims=optflow_fw, optflow_bw_ims=optflow_bw)
    #

def run(ims_dict, vidnames, seg_out_folder, optflow_fw_dict=None, optflow_bw_dict=None):
    '''
    Parameters:
        ims_dict: dict{vidname - str: ndarray(n_frames, sy, sx, 3) of ui8}
        vidnames: list of str; e.g., ['bear', 'blackswan']
        seg_out_folder: str; output path for the .h5 packages, e.g., '/home/my_home/git/ijcnn24_ivos_gnn_lp/preprocessed_data/segmentation/'
        (OPTIONAL) optflow_fw_dict, optflow_bw_dict: dict{vidname - str: ndarray(n_imgs-1, 480, 854, 2:[dy, dx]) of fl16}
    '''
    os.makedirs(seg_out_folder, exist_ok=True)

    vidnames_to_process = []
    for vidname in vidnames:
        fpath_out = os.path.join(seg_out_folder, 'ssn_' + str(vidname) + '.h5')
        if os.path.isfile(fpath_out):
            print("    Archive for video '" + vidname + "' found, skipping...")
        else:
            vidnames_to_process.append(vidname)

    for vidname in vidnames_to_process:
        optflow_fw = None if optflow_fw_dict is None else optflow_fw_dict[vidname]
        optflow_bw = None if optflow_bw_dict is None else optflow_bw_dict[vidname]
        _run_for_single_video(vidname, ims_dict[vidname], seg_out_folder, optflow_fw=optflow_fw, optflow_bw=optflow_bw)
    