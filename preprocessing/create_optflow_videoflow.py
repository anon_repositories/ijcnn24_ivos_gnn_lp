
#   Run VideoFlow optical flow estimation to videos and save results as .h5 packages.
#   VideoFlow optical flow estimation:
#       Shi et al.: VideoFlow: Exploiting Temporal Cues for Multi-frame Optical Flow Estimation
#       https://github.com/XiaoyuShi97/VideoFlow

import os
import sys
from argparse import Namespace 
import h5py
import numpy as np
import torch

import config as Config

VIDEOFLOW_MULTIFRAME_MODEL_PATH = os.path.join(Config.PATH_VIDEOFLOW_ROOT_FOLDER, 'VideoFlow_ckpt/MOF_sintel.pth')
sys.path.append(Config.PATH_VIDEOFLOW_ROOT_FOLDER)

from core.utils.utils import InputPadder, forward_interpolate
from core.Networks.MOFNetStack.network import MOFNet

CREATE_VIDEO = False

def compute_occlusions(flow_fw, flow_bw):
    '''
    Parameters:
        flow_fw, flow_bw: ndarray(sy, sx, 2:[dy, dx]) of fl32
    Returns:
        occl_fw, occl_bw: ndarray(sy, sx) of bool_
    '''
    assert flow_fw.shape == flow_bw.shape
    assert flow_fw.shape[2:] == (2,)
    NOISE_REDUCTION_BLOB_RADIUS = 0.1
    imsize = np.asarray(flow_fw.shape[:2], dtype=np.int32)

    occl_fw = np.empty(flow_fw.shape[:2], dtype=np.bool_)
    occl_bw = occl_fw.copy()

    vecs_to_2d_dict = {}
    occl_masks_dict = {}
    for direction in ['fw', 'bw']:
        flow_fwd_im = flow_fw if direction == 'fw' else flow_bw

        vecs_to = np.zeros_like(flow_fwd_im)    # (box_y, box_x, 2)
        indices = np.indices(flow_fwd_im.shape[:2], dtype=np.float64)
        vecs_to[:,:,0] = indices[0]
        vecs_to[:,:,1] = indices[1]
        vecs_to += flow_fwd_im
        vecs_to_2d_dict[direction] = vecs_to  # should not modify 'vecs_to' array after this line
        vecs_to = vecs_to[None,:,:,:]
        vecs_to = np.tile(vecs_to, (5,1,1,1))
        vecs_to[1] += np.asarray([NOISE_REDUCTION_BLOB_RADIUS,NOISE_REDUCTION_BLOB_RADIUS], dtype=np.float64)
        vecs_to[2] += np.asarray([-NOISE_REDUCTION_BLOB_RADIUS,NOISE_REDUCTION_BLOB_RADIUS], dtype=np.float64)
        vecs_to[3] += np.asarray([NOISE_REDUCTION_BLOB_RADIUS,-NOISE_REDUCTION_BLOB_RADIUS], dtype=np.float64)
        vecs_to[4] += np.asarray([-NOISE_REDUCTION_BLOB_RADIUS,-NOISE_REDUCTION_BLOB_RADIUS], dtype=np.float64)
        vecs_to = vecs_to.reshape((-1, 2))    # (box_y*box_x(*5), 2)
        vecs_to = np.around(vecs_to).astype(np.int32)
        valid_inds = np.all(vecs_to >= 0, axis=1) & np.all(vecs_to < imsize, axis=1)    # (box_y*box_x,)
        vecs_to = vecs_to[valid_inds]
        
        # occlusion mask
        mask = np.ones(flow_fwd_im.shape[:2], dtype=np.bool_)
        mask[vecs_to[:,0], vecs_to[:,1]] = 0
        occl_masks_dict[direction] = mask

    # set those pixels zero in the mask from which inverse optflow vectors point out of frame
    for direction in ['fw', 'bw']:
        inv_direction = 'bw' if direction == 'fw' else 'fw'
        vecs_to_inv = vecs_to_2d_dict[inv_direction]
        invalid_mask = np.any(vecs_to_inv < 0, axis=-1) | np.any(vecs_to_inv >= imsize-1, axis=-1)
        mask = occl_masks_dict[direction]
        mask[invalid_mask] = 0

    occl_fw, occl_bw = occl_masks_dict['fw'], occl_masks_dict['bw']
    return occl_fw, occl_bw

def run_and_save_optflow_occl(fpath_out, vidname, ims_arr, model, device):
    '''
    Parameters:
        fpath_out: str, e.g., '/home/my_home/git/ijcnn24_ivos_gnn_lp/cached_data/preprocessed_data/optflow/optflow_videoflow_bear.h5'
        vidname: str
        ims_arr: ndarray(n_imgs, 480, 854, 3:BGR) of uint8
        model: PyTorch Model
        device: Torch.device
    Returns:
        flows, inv_flows: ndarray(n_imgs-1, 480, 854, 2:[dy, dx]) of fl16
        occls, inv_occls: ndarray(n_imgs-1, 480, 854) of bool_
    '''
    SEQ_LEN = 8
    assert SEQ_LEN >= 5

    flows, inv_flows = [], []
    with torch.no_grad():
        
        # For a sequence of n frames (fr#0 .. fr#n-1), VideoFlow MOF model outputs (n-2)*2 frames of optical flow predictions:
        #   fr#1 -> fr#2, ... ,fr#n-2 -> fr#n-1, fr #1 -> fr #0, ... fr#n-2 -> n-3
        # duplicating first and last frames in the following line to be able to get fr#0 -> fr#1 and fr#n-1 -> fr#n-2
        ims_arr = np.pad(ims_arr, ((1,1), (0,0), (0,0), (0,0)), mode='edge')
        for fr_offset in range(0, ims_arr.shape[0], SEQ_LEN-2):
            fr_end_offset = min(ims_arr.shape[0], fr_offset+SEQ_LEN)
            fr_end_offset = fr_end_offset if ims_arr.shape[0]-fr_end_offset > 4 else ims_arr.shape[0]   # if last batch would be very short, concatenate it to the penultimate batch
            ims_seq = np.copy(ims_arr[fr_offset:fr_end_offset,:,:,::-1])      # ::-1 for BGR -> RGB, copy because tensors do not support negative stride
            ims_t = torch.from_numpy(ims_seq).permute(0, 3, 1, 2).float()[None,:,:,:,:].to(device)   # (B=1, N=SEQ_LEN, C, Y, X)
            padder = InputPadder(ims_t.shape)
            ims_t = padder.pad(ims_t)
            flow_pre, _ = model(ims_t)
            flow_pre = padder.unpad(flow_pre)[0].detach().permute(0, 2, 3, 1)
            flow_pre = flow_pre.cpu().numpy()                                          # (N=(SEQ_LEN-1)*2 or less, Y, X, C)
            flow_fw = flow_pre[:flow_pre.shape[0]//2,:,:,:]
            flow_bw = flow_pre[flow_pre.shape[0]//2:,:,:,:]
            assert flow_fw.shape[0] == flow_bw.shape[0]
            flows.append(flow_fw)
            inv_flows.append(flow_bw)
            del ims_seq, ims_t, flow_pre, flow_fw, flow_bw                     # free gpu memory before next iter
            if fr_end_offset == ims_arr.shape[0]:
                break

    flows = np.concatenate(flows, axis=0)[:-1,:,:,:]          # fr#0 -> fr#1, ..., fr#n-2 -> fr#n-1
    inv_flows = np.concatenate(inv_flows, axis=0)[1:,:,:,:]   # fr#1 -> fr#0, ..., fr#n-1 -> fr#n-2
    flows = flows[...,::-1]             # restore [dy, dx] order along last axis
    inv_flows = inv_flows[...,::-1]     # restore [dy, dx] order along last axis

    # compute occlusions
    occls, inv_occls = [], []
    for fr_idx in range(flows.shape[0]):
        occl, inv_occl = compute_occlusions(flows[fr_idx].astype(np.float64), inv_flows[fr_idx].astype(np.float64))
        occls.append(occl)
        inv_occls.append(inv_occl)

    # stack results and save
    occls = np.stack(occls, axis=0)
    inv_occls = np.stack(inv_occls, axis=0)

    of_h5 = h5py.File(fpath_out, 'w')
    of_h5.create_dataset("offset_yx", data=np.array([0., 0.], dtype=np.float32))
    of_h5.create_dataset("orig_vidsize_yx", data=np.array([480, 854], dtype=np.float32))
    of_h5.create_dataset("flow_run_size_yx", data=np.array([480, 854], dtype=np.float32))
    of_h5.create_dataset('flows', data=flows.astype(np.float16), compression="gzip")
    of_h5.create_dataset('inv_flows', data=inv_flows.astype(np.float16), compression="gzip")
    of_h5.create_dataset('occls', data=occls, compression="gzip")
    of_h5.create_dataset('inv_occls', data=inv_occls, compression="gzip")
    of_h5.close()

    return flows, inv_flows, occls, inv_occls

def load_optflow(base_folder_path, vidname, fname_prefix):
    '''
    Loads optical flow archives created previously.
    Parameters:
        base_folder_path: str
        vidname: str
        fname_prefix: str
    Returns:
        of_fw, of_bw: ndarray(n_imgs-1, 480, 854, 2:[dy, dx]) of fl16
        occl_fw, occl_bw: ndarray(n_imgs-1, 480, 854) of bool_
    '''
    flows_h5_path = os.path.join(base_folder_path, fname_prefix + vidname + '.h5')
    h5f = h5py.File(flows_h5_path, 'r')
    of_fw = h5f['flows'][:].astype(np.float16, copy=False)
    of_bw = h5f['inv_flows'][:].astype(np.float16, copy=False)
    occl_fw = h5f['occls'][:].astype(np.bool_, copy=False)
    occl_bw = h5f['inv_occls'][:].astype(np.bool_, copy=False)
    h5f.close()
    return of_fw, of_bw, occl_fw, occl_bw

def get_videoflow_config():
    '''
    Returns:
        argparse.Namespace; TODO used instead of yacs.config.CfgNode (originally yacs is used in VideoFlow/configs/*.py)
    '''
    _CN = Namespace()
    _CN.name = ''
    _CN.suffix =''
    _CN.gamma = 0.85
    _CN.max_flow = 400
    _CN.batch_size = 8
    _CN.sum_freq = 100
    _CN.val_freq = 100000000
    _CN.image_size = [432, 960]
    _CN.add_noise = False
    _CN.use_smoothl1 = False
    _CN.critical_params = []
    _CN.network = 'MOFNetStack'
    _CN.restore_ckpt = "PATH_TO_FINAL/final"
    _CN.mixed_precision = True
    _CN.input_frames = 5
    _CN.filter_epe = False

    _CN.MOFNetStack = Namespace()
    _CN.MOFNetStack.pretrain = True
    _CN.MOFNetStack.Tfusion = 'stack'
    _CN.MOFNetStack.cnet = 'twins'
    _CN.MOFNetStack.fnet = 'twins'
    _CN.MOFNetStack.down_ratio = 8
    _CN.MOFNetStack.feat_dim = 256
    _CN.MOFNetStack.corr_fn = 'default'
    _CN.MOFNetStack.corr_levels = 4
    _CN.MOFNetStack.mixed_precision = True
    _CN.MOFNetStack.context_3D = False

    _CN.MOFNetStack.decoder_depth = 12
    _CN.MOFNetStack.critical_params = ["cnet", "fnet", "pretrain", 'corr_fn', "Tfusion", "corr_levels", "decoder_depth", "mixed_precision"]

    return _CN

def count_videoflow_model_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)

def run(ims_dict, out_folder, out_fname_prefix, device):
    '''
    Parameters:
        ims_dict: dict{str - vidname: ndarray(n_imgs, 480, 854, 3:RGB) of uint8}
        out_folder: str; output path for single .h5 package, e.g., '/home/my_home/git/ijcnn24_ivos_gnn_lp/cached_data/preprocessed_data/optflow/'
        out_fname_prefix: str; e.g., 'optflow_videoflow_'
        device: str
    Returns:
        of_fw_dict, of_bw_dict: dict{vidname - str: ndarray(n_imgs-1, 480, 854, 2:[dy, dx]) of fl16}
        occl_fw_dict, occl_bw_dict: dict{vidname - str: ndarray(n_imgs-1, 480, 854) of bool_}
    '''
    videoflow_cfg = get_videoflow_config()

    model = torch.nn.DataParallel(MOFNet(videoflow_cfg.MOFNetStack))
    model.load_state_dict(torch.load(VIDEOFLOW_MULTIFRAME_MODEL_PATH))

    model.to(device)
    model.eval()

    of_fw_dict, of_bw_dict, occl_fw_dict, occl_bw_dict = {}, {}, {}, {}
    os.makedirs(out_folder, exist_ok=True)
    for vidname, ims_arr in ims_dict.items():
        assert ims_arr.shape[1:] == (480, 854, 3)
        fpath_out = os.path.join(out_folder, out_fname_prefix + vidname + '.h5')
        if os.path.isfile(fpath_out):
            print("    Archive for video '" + vidname + "' found, loading...")
            of_fw_dict[vidname], of_bw_dict[vidname], occl_fw_dict[vidname], occl_bw_dict[vidname] = \
                                            load_optflow(out_folder, vidname, out_fname_prefix)
        else:
            print("    Processing video '" + vidname + "'...")
            of_fw_dict[vidname], of_bw_dict[vidname], occl_fw_dict[vidname], occl_bw_dict[vidname] = \
                                            run_and_save_optflow_occl(fpath_out, vidname, ims_arr, model, device)

    return of_fw_dict, of_bw_dict, occl_fw_dict, occl_bw_dict

