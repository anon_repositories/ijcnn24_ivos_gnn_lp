
# 1-NN classifier implemented in PyTorch, interface uses CPU NumPy ndarrays.
# Designed to be compatible with feature vectors extracted by SSN Superpixel (Jampani et al.): using L2 distance.

import torch
import torch.nn.functional as tnnF

class NearestNeighborLabelModel():

    '''
    Member fields:
        target_device: str
        max_tensor_size: int; specify a lower value to temporary reduce memory consumption during evaluation
        n_categories: int

        fitted_xs: T(n_fitted_points, n_features) of fl32; all data points
        fitted_ys: T(n_fitted_points) of i64; labels assigned to 'fitted_xs'
    '''

    def __init__(self, target_device, max_tensor_size=int(1e8)):
        self.clear()
        self.target_device = target_device
        assert max_tensor_size >= int(1e7)
        self.max_tensor_size = max_tensor_size

    def clear(self):
        '''
        Clears fitted samples.
        '''
        self.fitted_xs = None
        self.fitted_ys = None
        self.n_categories = None

    def reset(self, n_categories):
        '''
        Sets number of categories for next fit.
        Parameters:
            n_categories: int
        '''
        assert self.n_categories is None, "Call clear() before resetting n_categories!"
        assert n_categories >= 2
        self.n_categories = n_categories

    def fit(self, xs, ys):
        '''
        Stores given samples.
        Parameters:
            xs: ndarray(n_fitted_points, n_features) of fl32; the feature vectors for each data point
            ys: ndarray(n_fitted_points) of i64; the true labels of each data point
        '''
        assert xs.ndim == 2
        assert ys.shape == xs.shape[:1]
        self.fitted_xs = torch.from_numpy(xs).to(device=self.target_device, dtype=torch.float32)
        self.fitted_ys = torch.from_numpy(ys).to(device=self.target_device, dtype=torch.int64)

    def predict(self, eval_xs, return_probs=True):
        '''
        Evaluate 'eval_xs' samples. Returns the category of the nearest fitted point.
            Returns a one-hot vector if 'return_probs' is True.
        Parameters:
            eval_xs: ndarray(n_points_to_eval, n_features) of fl32; the feature vectors for each data point
            return_probs: bool;
        Returns:
            ys_pred: ndarray(n_points_to_eval, n_categories) of fl32 (IF return_probs == True)
                     ndarray(n_points_to_eval,) of i64        (IF return_probs == False)
        '''
        n_points_to_eval = eval_xs.shape[0]
        eval_xs = torch.from_numpy(eval_xs).to(device=self.target_device, dtype=torch.float32)

        # create prediction
        n_fitted_samples, n_features = self.fitted_xs.shape
        batch_size = max(1, int(self.max_tensor_size / (n_fitted_samples*n_features)))
        ys_pred_batches = []
        for batch_offset_idx in range(0, n_points_to_eval, batch_size):
            eval_xs_batch = eval_xs[batch_offset_idx:batch_offset_idx+batch_size,:]   # (eval_batch_size, n_features)
            dists_batch = self._outer_L2_square(self.fitted_xs, eval_xs_batch)    # (n_fitted_points, eval_batch_size) fl32
            dists_batch_min_idxs = torch.argmin(dists_batch, dim=0)   # (eval_batch_size,) each
            ys_pred_batch = self.fitted_ys[dists_batch_min_idxs]
            ys_pred_batches.append(ys_pred_batch)

        ys_pred = torch.cat(ys_pred_batches, dim=0)
        if return_probs is True:
            ys_pred = tnnF.one_hot(ys_pred, num_classes=self.n_categories).to(torch.float32)    # (n_points_to_eval,) fl32

        ys_pred = ys_pred.to('cpu').numpy()
        return ys_pred

    def _outer_L2_square(self, points0, points1):
        '''
        Returns all pairwise squared L2 distances between points of 'points0' and 'points1'.
        Parameters:
            points0: T(n_points0, n_features) of fl32
            points1: T(n_points1, n_features) of fl32
        Returns:
            T(n_points0, n_points1) of fl32
        '''
        n_points0, n_features = points0.shape
        n_points1, _ = points1.shape
        assert points1.shape[1] == n_features
        assert points0.dtype == points1.dtype == torch.float32
        return torch.sum(torch.square(points0[:,None,:] - points1[None,:,:]), dim=2)   # dot product with self is slower

