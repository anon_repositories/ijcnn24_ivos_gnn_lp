
#   GatedGCN network customzied for label propagation.
#       based on
#           Dwivedi et al. 2020, "Benchmarking Graph Neural Networks",
#           and its implementation: https://github.com/graphdeeplearning/benchmarking-gnns.git
#       AND
#           Bresson et al. 2017, "Residual Gated Graph ConvNets"

import torch
import torch.nn as nn
import torch.nn.functional as F

from label_estimation.gated_gcn_layer_labelprop import GatedGCNLayer_LabelProp

class LabelPropGatedGCN(nn.Module):

    def __init__(self, net_params):
        super().__init__()
        in_dim_edge = net_params['in_dim_edge']
        in_dim_node = net_params['in_dim_node']
        hidden_dim = net_params['hidden_dim']
        out_dim = net_params['out_dim']
        dropout = net_params['dropout']
        n_layers = net_params['L']
        self.device = net_params['device']
        self.readout = net_params['readout']
        self.graph_norm = net_params['graph_norm']
        self.batch_norm = net_params['batch_norm']
        self.residual = net_params['residual']
        self.norm_adj_att_scores = net_params['norm_adj_att_scores']
        self.n_classes = net_params['n_classes']
        self.loss_name = net_params['loss_name']

        # h input embedding
        self.embedding_h = nn.Linear(in_dim_node, hidden_dim)
        self.embedding_e = nn.Linear(in_dim_edge, hidden_dim)

        # labelprop weight normalization for last layer is enabled with p_rescale=True, disabled with p_rescale=(layer_idx < n_layers-1)
        self.layers = nn.ModuleList([ GatedGCNLayer_LabelProp(hidden_dim, hidden_dim, dropout, self.graph_norm,
                        self.batch_norm, self.residual, norm_adj_att_scores=self.norm_adj_att_scores, \
                        p_dim=self.n_classes, p_rescale=(layer_idx < n_layers-1)) for layer_idx in range(n_layers) ])


    def forward(self, g, h, e, snorm_n, snorm_e, p, p2, p3):
        h = self.embedding_h(h)
        e = self.embedding_e(e)

        # convnets
        for conv in self.layers:
            h, e, p = conv(g, h, e, snorm_n, snorm_e, p, p2, p3)
        
        g.ndata['p'] = p
        return p
        
    def loss(self, pred, label, cat_weights=None):
        # pred: (n_samples, n_cats) of fl32
        # label: (n_samples,) i32
        # cat_weights: None OR (n_batches, n_cats)
        if self.loss_name == 'ce':
            loss = nn.CrossEntropyLoss()(pred, label)
        elif self.loss_name == 'weighted_ce':
            assert cat_weights is not None
            # only for a batch size of 1
            cat_weights = cat_weights.reshape(-1)
            loss = nn.CrossEntropyLoss(weight=cat_weights)(pred, label)
        elif self.loss_name == 'miou':
            # only for a batch size of 1
            loss = MeanIOULoss(apply_softmax=True, only_fg=False)(pred, label)
        elif self.loss_name == 'miou_fg_only':
            # only for a batch size of 1
            assert False, "apply softmax should be True"
            loss = MeanIOULoss(apply_softmax=False, only_fg=True)(pred, label)
        elif self.loss_name == 'mse':
            loss = nn.MSELoss()(pred, label)
        elif self.loss_name == 'lovasz':
            loss = LovaszSoftmax()(pred, label)
        else:
            assert False
        return loss


class MeanIOULoss(nn.Module):

    # Rahman et al. 2016, "Optimizing Intersection-Over-Union in Deep Neural Networks for Image Segmentation"

    def __init__(self, apply_softmax=False, only_fg=False):
        super(MeanIOULoss, self).__init__()
        self.apply_softmax = apply_softmax
        self.only_fg = only_fg

    def forward(self, pred, label):
        # pred: (n_samples, n_cats) of fl32
        # label: (n_samples,) of i32

        # cat labels to one-hot
        n_samples, n_cats = pred.size()
        n_cats_present = label.detach().max()+1
        label_onehot = torch.zeros_like(pred).scatter_(dim=1, index=label.long().view(n_samples, 1), \
                                                       src=torch.ones_like(pred))
        # apply softmax on predictions if needed
        if self.apply_softmax:
            pred = F.softmax(pred, dim=1)

        # if 'only_fg' enabled, do not compute IoU for category #0 (background)
        if self.only_fg:
            pred = pred[:,1:]
            label_onehot = label_onehot[:,1:]
            n_cats_present -= 1

        # intersection / union
        inter = pred * label_onehot
        union = pred + label_onehot - inter
        inter = inter.sum(dim=0)
        union = union.sum(dim=0)
        iou = inter / (union + 1e-8)
        iou = iou[:n_cats_present]
        return 1. - iou.mean()

class LovaszSoftmax(nn.Module):

    # Berman et al., 2017, "The Lovasz-Softmax loss: A tractable surrogate for the optimization of the intersection-over-union measure in neural networks"
    #     based on code: https://github.com/bermanmaxim/LovaszSoftmax/blob/master/pytorch/lovasz_losses.py

    def __init__(self):
        from torch.autograd import Variable

    def forward(self, pred, label):
        # pred: (n_samples, n_cats) of fl32
        # label: (n_samples,) of i32
        n_samples, n_cats = pred.size()
        n_cats_present = label.detach().max()+1
        losses = []
        for c in range(n_cats_present):
            fg = (label == c).float() # foreground for class c
            class_pred = pred[:, c]
            errors = (Variable(fg) - class_pred).abs()
            errors_sorted, perm = torch.sort(errors, 0, descending=True)
            perm = perm.data
            fg_sorted = fg[perm]
            losses.append(torch.dot(errors_sorted, Variable(LovaszSoftmax.lovasz_grad(fg_sorted))))
        return torch.mean(torch.stack(losses))

    @staticmethod
    def lovasz_grad(gt_sorted):
        """
        Computes gradient of the Lovasz extension w.r.t sorted errors
        See Alg. 1 in paper
        """
        p = len(gt_sorted)
        gts = gt_sorted.sum()
        intersection = gts - gt_sorted.float().cumsum(0)
        union = gts + (1 - gt_sorted).float().cumsum(0)
        jaccard = 1. - intersection / union
        if p > 1: # cover 1-pixel case
            jaccard[1:p] = jaccard[1:p] - jaccard[0:-1]
        return jaccard

