
# Runner script to generate dataset from GNN label predicitons for pixel-level label refinement learning

import config as Config
import sys
sys.path.append(Config.PATH_DAVIS_INTERACTIVE_TRAINING)

import os
import numpy as np
import pickle

from cache_manager import CacheManager
from datasets import DAVIS17
import util.davis_utils as DavisUtils
import util.imutil as ImUtil
import util.util as Util
import util.featuregen as FeatureGen

from seed_propagation.basic_optflow_seed_propagation import BasicOptflowSeedPropagation
from label_estimation.gnn_label_estimation import GNNLabelEstimation
from label_estimation.nearestneighbor_label_model import NearestNeighborLabelModel
from davisinteractive.session import DavisInteractiveSession

'''
    Saves DAVIS session data with GNN predictions to .pkl archives (one archive per session), each containing:

        list(n_steps) of dict{
                            'curr_annot_fr_idx': int,
                            'new_scribbles': dict{lab - int: list(n_scribbles_with_lab) of ndarray(n_points_in_scribble, 2:[y,x]) of int32},
                            'davis_state_prev_preds': ndarray(n_seg, p_dim) fl32,
                            'davis_state_seed_prev': dict{fr_idx - int: seed_arr - ndarray(n_seeds, 3:[py, px, lab] of i32}
                            'davis_state_seed_prop_prev': dict{fr_idx - int: seed_arr - ndarray(n_seeds, 3:[py, px, lab] of i32}
                        };
'''

if __name__ == '__main__':

    N_ITERS = 50    # in 1 iteration: for all dataset splits a DAVIS iterator 
                     #    is started and it goes through the whole split (3 annotation session per video)
    GNN_CHECKPOINT_FPATH = '/home/my_home/git/ijcnn24_ivos_gnn_lp/trained_models/gnn_model.pkl'

    SPLIT_NAMES = ['train', 'val']
    DAVIS_max_time_per_interaction = 30 # Maximum time per interaction per object (in seconds)
    DAVIS_metric = 'J'      # Metric to optimize

    # Total time available to interact with a sequence and an initial set of scribbles
    DAVIS_max_time = Config.REFINEMENT_DATASET_GEN_N_STEPS * DAVIS_max_time_per_interaction # Maximum time per object
    assert DAVIS_metric in ['J', 'F', 'J_AND_F']
    assert N_ITERS >= 1

    print("CONFIG --->")
    print({var: Config.__dict__[var] for var in dir(Config) if not var.startswith("__")})
    print("<--- CONFIG")

    N_VIDS_IN_MEMORY_LIMIT = 3
    cache_manager = CacheManager(n_vids_in_memory_limit=N_VIDS_IN_MEMORY_LIMIT, loading_protocol='cache_only')
    DatasetClass = CacheManager.get_dataset_class()
    all_vidnames = [vidname for split_name in SPLIT_NAMES \
                                    for vidname in DatasetClass.get_video_set_vidnames('full (60+30)', split_name)]
                                                   
    print("Loading all videos & data in order to find maximum n_labels value.")
    cache_manager.ensure_cache_exists(all_vidnames)
    n_labels_max = 0
    for vidname in all_vidnames:
        n_labels_max = max(n_labels_max, cache_manager.create_videodata_obj_reference(vidname).get_data('n_labels'))
        cache_manager.release_videodata_obj_reference(vidname)
    cache_manager.set_loading_protocol('gnn_traineval')

    # looping infinitely, stop script by killing the process after a while
    for iter_idx in range(N_ITERS):
        for split_name in SPLIT_NAMES:
            print(f"Refinement dataset collection, iter #{iter_idx} of {N_ITERS} started on dataset split '{split_name}'")
            
            # INIT LabelModel & SeedProp
            seedprop_alg = BasicOptflowSeedPropagation(cache_manager) if Config.USE_SEEDPROP is True else None
            lab_model_init_fn = lambda: NearestNeighborLabelModel(target_device='cuda')
            lab_model = lab_model_init_fn()

            # INIT LabelEstimation, load trained GNN model
            lab_est = GNNLabelEstimation(label_model=lab_model, multiclass_n_cats=n_labels_max, seedprop_alg=seedprop_alg)
            lab_est.load_gnn_checkpoint(GNN_CHECKPOINT_FPATH)

            with DavisInteractiveSession(host='localhost',
                                        user_key=None,
                                        davis_root=Config.PATH_DAVIS_ROOT_FOLDER,
                                        subset=split_name,
                                        shuffle=False,
                                        max_time=DAVIS_max_time,
                                        max_nb_interactions=Config.REFINEMENT_DATASET_GEN_N_STEPS,
                                        metric_to_optimize=DAVIS_metric,
                                        report_save_dir=None) as sess:
                curr_vidname = None
                davis_iter = sess.scribbles_iterator()

                # iterate DAVIS benchmark
                annotated_fr_idxs = []
                seq_idx = 0
                step_idx = -1
                for vidname, scribble, is_new_sequence in davis_iter:
                    step_idx += 1

                    # on new video, load video data
                    if vidname != curr_vidname:
                        seq_idx = -1
                        if curr_vidname is not None:
                            cache_manager.release_videodata_obj_reference(curr_vidname)
                        curr_vidname = vidname
                        curr_videodata = cache_manager.create_videodata_obj_reference(curr_vidname)
                        print("    sequence:", curr_vidname)
                        assert is_new_sequence

                        # init/reset label estimation, seedprop
                        lab_est.set_prediction_video(vidname=vidname, videodata=curr_videodata)
                        
                    # on new sequence (same or new video, restarted labeling session)
                    if is_new_sequence:
                        data_to_save = []
                        prev_scribble_dict = None
                        curr_scribble_dict = None
                        annotated_fr_idxs = []
                        davis_state_prev_preds = None
                        davis_state_seed_hist = []
                        davis_state_seed_prop_hist = []
                        seq_idx += 1
                        step_idx = 0
                        prev_pred_label_im = None

                    # extract new scribbles
                    prev_scribble_dict = curr_scribble_dict
                    curr_scribble_dict = scribble
                    curr_annot_fr_idx, new_scribbles = DavisUtils.get_new_scribbles(vidname, prev_scribble_dict, curr_scribble_dict, (480, 854))
                    annotated_fr_idxs.append(curr_annot_fr_idx)

                    # convert new scribbles to seeds, use different algorithm if first step in current sequence
                    N_SEEDS_PER_CAT_INITIAL = 100
                    N_SEEDS_PER_CAT_LATER = 100

                    n_seeds_per_cat = N_SEEDS_PER_CAT_INITIAL if is_new_sequence else N_SEEDS_PER_CAT_LATER
                    curr_seed_points = DavisUtils.davis_scribbles2seeds_uniform(new_scribbles, (480, 854), n_seeds_per_cat, \
                                                                               generate_bg=is_new_sequence)  # (n_seeds, 3:[y, x, lab])
                    
                    # submit seeds to label estimation, generate predictions
                    assert curr_seed_points.shape[1:] == (3,)
                    fr_idxs = np.full(curr_seed_points.shape[0], dtype=np.int32, fill_value=curr_annot_fr_idx)
                    coords, labels = curr_seed_points[:,:2], curr_seed_points[:,2]

                    lab_est.set_prediction_davis_state(curr_annot_fr_idx, new_scribbles, \
                                                    davis_state_prev_preds, davis_state_seed_hist, davis_state_seed_prop_hist)
                    davis_state_prev_preds = lab_est.predict_all(return_probs=True)
                    davis_state_prev_preds_am = np.argmax(davis_state_prev_preds, axis=-1)

                    _, davis_state_seed_hist, davis_state_seed_prop_hist = lab_est.get_prediction_davis_state()
                    seg_im = curr_videodata.get_seg().get_seg_im(framewise_seg_ids=False)
                    pred_label_im = davis_state_prev_preds_am[seg_im]

                    # dataset collection, save if sequence ended
                    data_to_save.append({'curr_annot_fr_idx': curr_annot_fr_idx, 
                                         'new_scribbles': new_scribbles, 
                                         'davis_state_prev_preds': davis_state_prev_preds,
                                         'davis_state_seed_prev': davis_state_seed_hist[-1], 
                                         'davis_state_seed_prop_prev': davis_state_seed_prop_hist[-1]})
                    if step_idx == Config.REFINEMENT_DATASET_GEN_N_STEPS-1:
                        data_to_save_folder = os.path.join(Config.PATH_REFINEMENT_DATASET_GEN_FOLDER, curr_vidname)
                        os.makedirs(data_to_save_folder, exist_ok=True)
                        data_to_save_fname = curr_vidname + '_' + Util.get_base58_timestamp()[::-1] + '.pkl'  # unique name for file
                        data_to_save_fpath = os.path.join(data_to_save_folder, data_to_save_fname)
                        with open(data_to_save_fpath, 'wb') as data_to_save_fp:
                            pickle.dump(data_to_save, data_to_save_fp)
                        data_to_save = []

                    # produce frame query list, can choose different protocols to get frames to query next
                    assert Config.REFINEMENT_DATASET_GEN_FRAME_QUERY_METHOD in ['default', 'equidistant', 'choose_from_distant', 'random_uniform', 'random_linear_distance_prob']
                    if Config.REFINEMENT_DATASET_GEN_FRAME_QUERY_METHOD == 'default':
                        frames_to_query = None
                    elif Config.REFINEMENT_DATASET_GEN_FRAME_QUERY_METHOD == 'equidistant':
                        query_ratios = [0.5, 0.25, 0.75, 0.125, 0.375, 0.625, 0.875, 0.]
                        next_fr = int(query_ratios[len(annotated_fr_idxs)-1]*curr_videodata.get_seg().get_n_frames())
                        frames_to_query = [min(max(0, next_fr), curr_videodata.get_seg().get_n_frames()-1)]
                    elif Config.REFINEMENT_DATASET_GEN_FRAME_QUERY_METHOD == 'choose_from_distant':
                        from scipy.ndimage import distance_transform_cdt
                        frame_dists = np.ones((curr_videodata.get_seg().get_n_frames(),), dtype=np.int32)
                        frame_dists[annotated_fr_idxs] = 0
                        frame_dists = distance_transform_cdt(frame_dists)
                        frames_to_query = list(np.argsort(frame_dists)[(3*frame_dists.shape[0])//4:])
                        frames_to_query = frames_to_query
                    elif Config.REFINEMENT_DATASET_GEN_FRAME_QUERY_METHOD == 'random_uniform':
                        frames_not_queried_yet = np.setdiff1d(np.arange(pred_label_im.shape[0]), annotated_fr_idxs)
                        frames_to_query = [np.random.choice(frames_not_queried_yet)]
                    elif Config.REFINEMENT_DATASET_GEN_FRAME_QUERY_METHOD == 'random_linear_distance_prob':
                        from scipy.ndimage import distance_transform_cdt
                        frame_dists = np.ones(pred_label_im.shape[:1], dtype=np.int32)
                        frame_dists[annotated_fr_idxs] = 0
                        frame_dists = distance_transform_cdt(frame_dists)
                        frame_dists = frame_dists / float(np.sum(frame_dists))
                        frames_to_query = [np.random.choice(pred_label_im.shape[0], p=frame_dists)]
                        
                    # submit predicted masks to DAVIS benchmark, resize predictions if necessary
                    prev_pred_label_im = pred_label_im.copy()
                    if curr_vidname in DAVIS17.CUSTOM_IMSIZE_DICT.keys():
                        pred_label_im = ImUtil.fast_resize_video_nearest_singlech(pred_label_im, DAVIS17.CUSTOM_IMSIZE_DICT[curr_vidname])

                    DavisUtils.fix_label_image_error(vidname, pred_label_im)
                    sess.submit_masks(pred_label_im, next_scribble_frame_candidates=frames_to_query)

                # release data corresponding to the last video at the end of the session
                if curr_vidname is not None:
                    cache_manager.release_videodata_obj_reference(curr_vidname)

    print("Refinement dataset collection completed.")
