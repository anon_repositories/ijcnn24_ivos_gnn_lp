
#   Runner script to run the DAVIS Interactive benchmark on a trained GNN model together with a trained refinement model.

import sys
import config as Config
sys.path.append(Config.PATH_DAVIS_INTERACTIVE)

import os
import numpy as np
import torch

from cache_manager import CacheManager
from datasets import DAVIS17
import util.davis_utils as DavisUtils
import util.imutil as ImUtil
import util.featuregen as FeatureGen

from seed_propagation.basic_optflow_seed_propagation import BasicOptflowSeedPropagation
from label_estimation.gnn_label_estimation import GNNLabelEstimation
from label_estimation.nearestneighbor_label_model import NearestNeighborLabelModel
from davisinteractive.session import DavisInteractiveSession

def apply_refinement_model(ref_model, device, video_data, data_gen, y_in_seg):
    '''
    Parameters:
        ref_model: RefinementModel instance
        device: str; target device (e.g., 'cuda')
        video_data: VideoData
        data_gen: TODO some data generator type
        y_in_seg: ndarray(n_seg, n_cat) of fl32
    Returns:
        y_refined_labels: ndarray(n_frames, sy, sx) of int32;
    '''
    n_frames = video_data.get_seg().get_n_frames()
    n_segs = video_data.get_seg().get_n_segs_total()
    assert y_in_seg.shape[0] == n_segs
    assert y_in_seg.ndim == 2
    ref_model.eval()
    with torch.no_grad():
        y_refined_labels = np.empty(video_data.get_seg().get_shape(), dtype=np.int32)
        for fr_idx in range(n_frames):
            x_gpu, x_ds2_gpu, y_in_gpu = data_gen.create_pred_sample(video_data=video_data, fr_idx=fr_idx, y_in_seg=y_in_seg)
            y_pred_gpu, _ = ref_model.forward(x_gpu, y_in_gpu, x_ds2_gpu)   # T(sy, sx, n_cat) of fl32 PROBABILITIES (sum=1 along last dim), T(4, sy, sx) of fl32
            y_refined_labels[fr_idx,:,:] = y_pred_gpu.detach().cpu().numpy().argmax(axis=-1)

    return y_refined_labels


if __name__ == '__main__':

    #SET_NAME = 'debug val (0+3)'
    SET_NAME = 'full val (0+30)'

    print("CONFIG --->")
    print({var: Config.__dict__[var] for var in dir(Config) if not var.startswith("__")})
    print("<--- CONFIG")

    GNN_CHECKPOINT_FPATH = '/home/my_home/git/ijcnn24_ivos_gnn_lp/trained_models/gnn_model.pkl'
    REFINEMENT_CHECKPOINT_FPATH = '/home/my_home/git/ijcnn24_ivos_gnn_lp/trained_models/refinement_model.pkl'   # can be None

    N_VIDS_IN_MEMORY_LIMIT = 3
    cache_manager = CacheManager(n_vids_in_memory_limit=N_VIDS_IN_MEMORY_LIMIT, loading_protocol='cache_only')
    DatasetClass = CacheManager.get_dataset_class()
    test_vidnames = DatasetClass.get_video_set_vidnames(SET_NAME, 'val')
    
    print("Loading all videos & data in order to find maximum n_labels value.")
    cache_manager.ensure_cache_exists(test_vidnames)
    n_labels_max = 0
    for vidname in test_vidnames:
        n_labels_max = max(n_labels_max, cache_manager.create_videodata_obj_reference(vidname).get_data('n_labels'))
        cache_manager.release_videodata_obj_reference(vidname)
    cache_manager.set_loading_protocol('all_eval')

    # INIT LabelModel & SeedProp
    seedprop_alg = BasicOptflowSeedPropagation(cache_manager) if Config.USE_SEEDPROP is True else None
    lab_model_init_fn = lambda: NearestNeighborLabelModel(target_device='cuda')
    lab_model = lab_model_init_fn()

    # INIT LabelEstimation, load trained GNN model
    lab_est = GNNLabelEstimation(label_model=lab_model, multiclass_n_cats=n_labels_max, seedprop_alg=seedprop_alg)
    lab_est.load_gnn_checkpoint(GNN_CHECKPOINT_FPATH)

    # Load refinement model
    chekpoint_name_to_load_refinement = 'None    '
    if REFINEMENT_CHECKPOINT_FPATH is not None:
        from refinement.refinement_data_generator_async import RefinementDataGenerator_Async
        from refinement.refinement_model import RefinementModel

        REF_DEVICE = 'cuda:0'
        ref_test_iter = RefinementDataGenerator_Async(vidnames=test_vidnames, target_device=REF_DEVICE, \
                    n_samples_per_chunk=100, augmenter=None, no_parallelism=True)   # no_parallelism must be True, otherwise it will spawn worker threads immediately

        n_ref_in_channels = ref_test_iter.get_n_xfeatures()
        n_ref_ds2_in_channels = 18
        ref_model = RefinementModel(device=REF_DEVICE, loss_name=Config.REFINEMENT_LOSS_NAME, n_in_channels=n_ref_in_channels, \
                            n_tr_iters_per_scale=Config.REFINEMENT_N_TRAINING_ITER_PER_SCALE, \
                            n_eval_iters_per_scale=Config.REFINEMENT_N_EVAL_ITER_PER_SCALE, \
                            n_ds2_in_channels=n_ref_ds2_in_channels, \
                            n_channels_mult=Config.REFINEMENT_CNN_N_CONV_CHANNELS_MULT, \
                            n_unet_scales=Config.REFINEMENT_CNN_N_SCALES, \
                            batch_norm=Config.REFINEMENT_BATCH_NORM, \
                            n_conv_layers_per_block=Config.REFINEMENT_N_CONV_LAYERS_PER_BLOCK, \
                            p_pred_power_k=Config.REFINEMENT_P_PRED_POWER_K, \
                            dropout_rate=Config.REFINEMENT_DROPOUT_RATE, \
                            two_channel_aff=Config.REFINEMENT_TWO_CHANNEL_AFFINITY)
        ref_model = ref_model.to(REF_DEVICE)
        ref_model.load_state_dict(torch.load(REFINEMENT_CHECKPOINT_FPATH))

    # ---------------------------- DAVIS Interactive Challenge - benchmark evaluation ----------------------------

    print("Running DAVIS benchmark on trained models...")
    os.makedirs(Config.PATH_DAVIS_BENCHMARK_REPORT_FOLDER, exist_ok=True)

    # Configuration used in the challenges
    DAVIS_max_nb_interactions = 8 # Maximum number of interactions 
    DAVIS_max_time_per_interaction = 30 # Maximum time per interaction per object (in seconds)

    # Total time available to interact with a sequence and an initial set of scribbles
    DAVIS_max_time = DAVIS_max_nb_interactions * DAVIS_max_time_per_interaction # Maximum time per object

    # Metric to optimize
    DAVIS_metric = 'J'
    assert DAVIS_metric in ['J', 'F', 'J_AND_F']

    with DavisInteractiveSession(host='localhost',
                            user_key=None,
                            davis_root=Config.PATH_DAVIS_ROOT_FOLDER,
                            subset='val',
                            shuffle=False,
                            max_time=DAVIS_max_time,
                            max_nb_interactions=DAVIS_max_nb_interactions,
                            metric_to_optimize=DAVIS_metric,
                            report_save_dir=Config.PATH_DAVIS_BENCHMARK_REPORT_FOLDER) as sess:

        curr_vidname = None
        davis_iter = sess.scribbles_iterator()

        # iterate DAVIS benchmark
        annotated_fr_idxs = []
        seq_idx = 0
        step_idx = -1
        for vidname, scribble, is_new_sequence in davis_iter:
            step_idx += 1

            # on new video, load video data
            if vidname != curr_vidname:
                seq_idx = -1
                if curr_vidname is not None:
                    cache_manager.release_videodata_obj_reference(curr_vidname)
                curr_vidname = vidname
                curr_videodata = cache_manager.create_videodata_obj_reference(curr_vidname)
                assert is_new_sequence

                # init/reset label estimation, seedprop
                lab_est.set_prediction_video(vidname=vidname, videodata=curr_videodata)
                    
            # on new sequence (same or new video, restarted labeling session)
            if is_new_sequence:
                prev_scribble_dict = None
                curr_scribble_dict = None
                annotated_fr_idxs = []
                davis_state_prev_preds = None
                davis_state_seed_hist = []
                davis_state_seed_prop_hist = []
                seq_idx += 1
                step_idx = 0

            # extract new scribbles
            prev_scribble_dict = curr_scribble_dict
            curr_scribble_dict = scribble
            curr_annot_fr_idx, new_scribbles = DavisUtils.get_new_scribbles(vidname, prev_scribble_dict, curr_scribble_dict, (480, 854))
            annotated_fr_idxs.append(curr_annot_fr_idx)

            lab_est.set_prediction_davis_state(curr_annot_fr_idx, new_scribbles, \
                                            davis_state_prev_preds, davis_state_seed_hist, davis_state_seed_prop_hist)
            davis_state_prev_preds = lab_est.predict_all(return_probs=True)
            davis_state_prev_preds_am = np.argmax(davis_state_prev_preds, axis=-1)

            _, davis_state_seed_hist, davis_state_seed_prop_hist = lab_est.get_prediction_davis_state()
            seg_im = curr_videodata.get_seg().get_seg_im(framewise_seg_ids=False)
            pred_label_im = davis_state_prev_preds_am[seg_im]

            # get metrics, run refinement in last step
            if step_idx == 7:

                # REFINEMENT: run refinement model if it is enabled and if last step
                if REFINEMENT_CHECKPOINT_FPATH is not None:
                    pred_label_im = apply_refinement_model(ref_model=ref_model, device=REF_DEVICE, video_data=curr_videodata, \
                                data_gen=ref_test_iter, y_in_seg=davis_state_prev_preds)

                n_labels = curr_videodata.get_data('n_labels')
                true_lab_im = curr_videodata.get_data('annot_im')
                pred_metric_mean_j_im_final = ImUtil.compute_pixelwise_labeling_error(true_lab_im, pred_label_im, n_labels)
                print(f"  sequence '{curr_vidname}' #{seq_idx} -- 8th step mean J: {pred_metric_mean_j_im_final}") # unofficial metric, check DAVIS report for official benchmark results

            # REFINEMENT: refinement end

            # submit predicted masks to DAVIS benchmark, resize predictions if necessary
            if curr_vidname in DAVIS17.CUSTOM_IMSIZE_DICT.keys():
                pred_label_im = ImUtil.fast_resize_video_nearest_singlech(pred_label_im, DAVIS17.CUSTOM_IMSIZE_DICT[curr_vidname])

            DavisUtils.fix_label_image_error(vidname, pred_label_im)
            sess.submit_masks(pred_label_im, next_scribble_frame_candidates=None)   # 'default' protocol for frame query

        # Get the global summary
        report_save_fpath = os.path.join(Config.PATH_DAVIS_BENCHMARK_REPORT_FOLDER, 'summary.json')
        summary = sess.get_global_summary(save_file=report_save_fpath)
