
# Data augmentation methods

import numpy as np
import torch
import torch.nn.functional as tnnF

class BasicAugmenter:

    # Functionality:
    #    random horizontal/vertical flip
    #    optical flow random rescale
    #    random multiplicative noise in x and x_ds2 

    def augment(self, x_gpu, x_ds2_gpu, y_in_gpu, y_true_gpu, im_bgr_npy):
        '''
        Augments the sample with basic transformations & noise.
        Parameters:
            x_gpu: T(sy, sx, x_channels) of fl32; channels: [lab:3, of_fw:2, of_bw:2]
            x_ds2_gpu: T(sy//2, sx//2, x_ds2_channels) of fl32; channels: [fmap:n_features]
            y_in_gpu: T(sy, sx, n_categories) of fl32; the label map to refine  (one-hot probabilities)
            y_true_gpu: T(sy, sx, n_categories) of fl32; the true label map (one-hot probabilities)
            im_bgr_npy: ndarray(sy, sx, 3:[BGR]) of uint8
        Returns:
            x_gpu: T(sy, sx, x_channels) of fl32; channels: [lab:3, of_fw:2, of_bw:2]
            x_ds2_gpu: T(sy//2, sx//2, x_ds2_channels) of fl32; channels: [fmap:n_features]
            y_in_gpu: T(sy, sx, n_categories) of fl32; the label map to refine  (one-hot probabilities)
            y_true_gpu: T(sy, sx, n_categories) of fl32; the true label map (one-hot probabilities)
            im_bgr_npy: ndarray(sy, sx, 3:[BGR]) of uint8
        '''
        assert x_gpu.ndim == x_ds2_gpu.ndim == y_in_gpu.ndim == y_true_gpu.ndim == im_bgr_npy.ndim == 3
        assert x_gpu.shape[-1] == 7
        device = x_gpu.device
        dtype_fl = x_gpu.dtype

        # flip
        flip_axes = []
        flip_axes = flip_axes + [0] if np.random.rand() < 0.5 else flip_axes
        flip_axes = flip_axes + [1] if np.random.rand() < 0.5 else flip_axes
        x_gpu = torch.flip(x_gpu, flip_axes)
        x_ds2_gpu = torch.flip(x_ds2_gpu, flip_axes)
        y_in_gpu = torch.flip(y_in_gpu, flip_axes)
        y_true_gpu = torch.flip(y_true_gpu, flip_axes)
        im_bgr_npy = np.flip(im_bgr_npy, flip_axes)

        # optical flow rescale
        r_optflow_rescale_yxyx = torch.tile(torch.rand(2, device=device, dtype=dtype_fl) * 4. - 2., (2,))         # uniform in -2. .. 2., same for matching fw & bw channels
        x_gpu[:,:,3:] = x_gpu[:,:,3:] * r_optflow_rescale_yxyx    # x_gpu is copied above, even if flip_axes == []

        # noise
        NOISE_STD = 0.1
        assert 0 <= NOISE_STD < 0.3
        r_x_noise_mul = torch.clamp(1. + torch.randn(x_gpu.shape, device=device, dtype=dtype_fl) * NOISE_STD, min=0.)
        r_x_ds2_noise_mul = torch.clamp(1. + torch.randn(x_ds2_gpu.shape, device=device, dtype=dtype_fl) * NOISE_STD, min=0.)
        x_gpu = x_gpu * r_x_noise_mul                   # x_gpu, x_ds2_gpu is copied above, even if flip_axes == []
        x_ds2_gpu = x_ds2_gpu * r_x_ds2_noise_mul

        return x_gpu, x_ds2_gpu, y_in_gpu, y_true_gpu, im_bgr_npy

