
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as tnnF

class RefinementIteration(nn.Module):

    def __init__(self, n_iters_per_scale):
        '''
        Parameters:
            n_iters_per_scale: list of ints; number of iterations for each scale from smallest towards full resolution; 
                                        e.g., [1,2,3] refers to 1 iterations at x4 downscaled affinity maps,
                                                      2 iterations at x2 and 3 iterations at full resolution
        '''
        super().__init__()
        assert 1 <= len(n_iters_per_scale) <= 5
        self.n_iters_per_scale = n_iters_per_scale
        

    def forward(self, y_in, aff_maps):
        '''
        Applies the affinity maps on 'y_in' 'self.n_iters_per_scale[i]' times in each scale #i. 
            See 'self.n_iters_per_scale' for details.
        Parameters:
            y_in: T(H, W, n_cat) fl32; sum=1 along last dim
            aff_maps: T(C=4, H, W) fl32; expecting all values to be in range [0,1].
        Returns:
            y_out: T(H, W, n_cat) fl32; sum=1 along last dim
        '''
        assert len(aff_maps.shape) == len(y_in.shape) == 3
        assert y_in.shape[:2] == aff_maps.shape[1:]
        assert aff_maps.shape[0] == 4
        y_in = y_in.permute(2,0,1)   # T(n_cat, H, W)

        y_orig_dss = [y_in]         # y_orig maps downscaled to [..., 4x, 2x, 1x]
        aff_maps_dss = [aff_maps]   # affinity maps downscaled to [..., 4x, 2x, 1x]
        for scale_idx in range(len(self.n_iters_per_scale)-1):
            y_orig_prev = y_orig_dss[0]
            aff_maps_prev = aff_maps_dss[0]
            ds_H, ds_W = y_orig_prev.shape[1] // 2, y_orig_prev.shape[2] // 2
            y_orig_dss.insert(0, tnnF.adaptive_avg_pool2d(y_orig_prev, (ds_H, ds_W)))    # insert smaller resolution to beginning of list
            aff_maps_dss.insert(0, tnnF.adaptive_avg_pool2d(aff_maps_prev, (ds_H, ds_W)))    # insert smaller resolution to beginning of list

        y_out = y_orig_dss[0]          # T(n_cat, H, W)
        for scale_idx, prop_count in enumerate(self.n_iters_per_scale):           # iterate different scale factors
            for prop_idx in range(prop_count):                  # iterate repetitions in a specific scale factor
                for aff_map, direction in zip(aff_maps_dss[scale_idx], ['N', 'E', 'S', 'W']):      # iterate the four propagation directions
                    y_out = self._apply_aff_map(y_orig_dss[scale_idx], y_out, aff_map, direction)

            if scale_idx != len(self.n_iters_per_scale)-1:          # if not full resolution
                us_H, us_W = y_orig_dss[scale_idx+1].shape[1:]
                y_out = tnnF.adaptive_avg_pool2d(y_out, (us_H, us_W))    # T(n_cat, H, W)

        y_out = y_out.permute(1,2,0)   # T(H, W, n_cat)
        return y_out

    
    def _apply_aff_map(self, y_orig, y_in, aff_map, direction):
        '''
        !!! EDIT: y_orig is disabled, y_in is used instead. !!!
        Applies the affinity map once on 'y_in' (and with 'y_orig' being the original label map).
        Parameters:
            y_orig: T(n_cat, H_ds, W_ds) fl32; sum=1 along first dim
            y_in: T(n_cat, H_ds, W_ds) fl32; sum=1 along first dim
            aff_map: T(H_ds, W_ds) fl32; expecting all values to be in range [0,1].
            direction: str; any of ['N', 'E', 'S', 'W'] representing propagation in north, east, ... directions
        Returns:
            y_out: T(n_cat, H, W) fl32; sum=1 along first dim
        '''
        assert direction in ['N', 'E', 'S', 'W']
        assert len(aff_map.shape)+1 == len(y_in.shape) == 3
        assert y_in.shape[1:] == aff_map.shape

        # pad aff maps, ys
        H, W = y_orig.shape[1:]
        y_orig = tnnF.pad(y_orig, (1,1, 1,1, 0,0), mode='constant', value=0.)       # T(n_cat, H+2, W+2)
        y_in = tnnF.pad(y_in, (1,1, 1,1, 0,0), mode='constant', value=0.)           # T(n_cat, H+2, W+2)
        aff_map = tnnF.pad(aff_map, (1,1, 1,1), mode='constant', value=0.)   # T(H+2, W+2)

        START_SLICE2, MID_SLICE2, END_SLICE2 = slice(None, -2), slice(1, -1), slice(2, None)    # [:-2], [1:-1], [2:]
        EMPTY_SLICE = slice(None)                                                               # [:]
        if direction in ['N', 'W']:
            wslice1 = (END_SLICE2, START_SLICE2)    # direction#1 prop 'from' idxs
            wslice2 = (END_SLICE2, MID_SLICE2)      # direction#2 prop 'from' idxs
            wslice3 = (END_SLICE2, END_SLICE2)      # direction#3 prop 'from' idxs
            mhalfslice1 = (EMPTY_SLICE, 0)          # prop 'to' idxs where only 2 'from' idx is valid
            mhalfslice2 = (EMPTY_SLICE, -1)         # prop 'to' idxs where only 2 'from' idx is valid
            mfullslice = (-1, EMPTY_SLICE)          # prop 'to' idxs where no 'from' idx is valid
        elif direction in ['S', 'E']:
            wslice1 = (START_SLICE2, START_SLICE2)
            wslice2 = (START_SLICE2, MID_SLICE2)
            wslice3 = (START_SLICE2, END_SLICE2)
            mhalfslice1 = (EMPTY_SLICE, 0)
            mhalfslice2 = (EMPTY_SLICE, -1)
            mfullslice = (0, EMPTY_SLICE)

        if direction in ['W', 'E']:                 # same as 'N' and 'S', but y and x axes are swapped
            wslice1, wslice2, wslice3, mhalfslice1, mhalfslice2, mfullslice = \
                            wslice1[::-1], wslice2[::-1], wslice3[::-1], mhalfslice1[::-1], mhalfslice2[::-1], mfullslice[::-1]

        # exclude masked items from mean (to take mean, sum is divided by 3 by default, except at borders)
        w_mul = torch.full((H, W), dtype=aff_map.dtype, device=aff_map.device, fill_value=1/3.)   # T(H, W)
        w_mul[mhalfslice1] = 1/2.
        w_mul[mhalfslice2] = 1/2.
        w_mul[mfullslice] = 0.

        w1mask = torch.ones((H, W), dtype=aff_map.dtype, device=aff_map.device)      # T(H, W)
        w1mask[mhalfslice1] = 0.
        w1mask[mfullslice] = 0.
        w1 = aff_map[wslice1] * w1mask * w_mul          # T(H, W), SW
        w2mask = torch.ones_like(w1mask)
        w2mask[mfullslice] = 0.
        w2 = aff_map[wslice2] * w2mask * w_mul         # T(H, W), S
        w3mask = torch.ones_like(w1mask)
        w3mask[mhalfslice2] = 0.
        w3mask[mfullslice] = 0.
        w3 = aff_map[wslice3] * w3mask * w_mul          # T(H, W), SE

        w0 = 1. - (w1 + w2 + w3)                  # T(H, W)
        y1 = y_in[(EMPTY_SLICE,) + wslice1]                     # T(n_cat, H, W)
        y2 = y_in[(EMPTY_SLICE,) + wslice2]                     # T(n_cat, H, W)
        y3 = y_in[(EMPTY_SLICE,) + wslice3]                     # T(n_cat, H, W)
        y0 = y_in[:, 1:-1,1:-1]                # T(n_cat, H, W)
        y_out = w0*y0 + w1*y1 + w2*y2 + w3*y3         # T(n_cat, H, W)

        return y_out
