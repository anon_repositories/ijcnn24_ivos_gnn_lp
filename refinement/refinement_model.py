
# A variant of the Spatial Propagation Network, Liu et al., 2017

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as tnnF

from refinement.refinement_iter import RefinementIteration
import util.util as Util
from refinement.unet import UNet_DS2_Input

class RefinementModel(nn.Module):

    def __init__(self, device, loss_name, n_in_channels, n_tr_iters_per_scale, n_eval_iters_per_scale, \
                        n_ds2_in_channels=None, \
                        n_channels_mult=16, n_unet_scales=3, batch_norm=True, n_conv_layers_per_block=2, \
                        p_pred_power_k=None, dropout_rate=.0, two_channel_aff=False):
        '''
        Parameters:
            device: str
            loss_name: str
            n_in_channels: int
            n_tr_iters_per_scale: list of ints; number of iterations for each scale from smallest towards full resolution during training; 
                                        e.g., [1,2,3] refers to 1 iterations at x4 downscaled affinity maps,
                                                      2 iterations at x2 and 3 iterations at full resolution

            n_eval_iters_per_scale: list of ints; number of iterations for each scale from smallest towards full resolution during evaluation; 
            n_ds2_in_channels: None OR int
            n_channels_mult: int
            n_unet_scales: int
            batch_norm: bool
            n_conv_layers_per_block: int
            p_pred_power_k: None or float
            dropout_rate: float
            two_channel_aff: bool
        '''
        super().__init__()
        self.device = device
        self.iter_mode_is_training = True
        self.n_in_channels = n_in_channels
        self.n_ds2_in_channels = n_ds2_in_channels
        assert (p_pred_power_k is None) or (p_pred_power_k >= 0.)
        self.p_pred_power_k = p_pred_power_k
        self.two_channel_aff = two_channel_aff
        assert loss_name in ['ce', 'miou']
        self.loss_name = loss_name
        if loss_name == 'miou':
            self.loss_expr = MeanIOULoss_Refinement(apply_softmax=False, only_fg=False)
        elif loss_name == 'ce':
            self.loss_expr = ce_loss_from_probabilities

        n_out_channels = 2 if self.two_channel_aff is True else 4
        self.aff_net = UNet_DS2_Input(n_in_channels, device=self.device, n_out_channels=n_out_channels, \
                                   n_ds2_in_channels=n_ds2_in_channels, n_channels_mult=n_channels_mult, \
                                   n_scales=n_unet_scales, batch_norm=batch_norm, \
                                   n_conv_layers_per_block=n_conv_layers_per_block, dropout_rate=dropout_rate)
        self.refinement_iter_training = RefinementIteration(n_tr_iters_per_scale)
        self.refinement_iter_eval = RefinementIteration(n_eval_iters_per_scale)

    def set_iter_mode(self, is_training):
        assert type(is_training) is bool
        self.iter_mode_is_training = is_training
        iter_mode_str = "training" if is_training else "eval"
        n_iters_per_scale = self.refinement_iter_training.n_iters_per_scale if is_training else self.refinement_iter_eval.n_iters_per_scale

    def forward(self, x, y_in, x_ds2=None):
        '''
        Parameters:
            x: T(sy, sx, n_in_channels) of fl32
            y_in: T(sy, sx, n_cat) fl32; sum=1 along last dim
            x_ds2: None OR T(sy//2, sx//2, n_in_ds2_channels) of fl32
        Returns:
            y_pred: T(sy, sx, n_cat) of fl32 PROBABILITIES (sum=1 along last dim)
            aff_maps: T(4, sy, sx) of fl32 VALUES IN [0,1];
        '''
        assert x.shape[2:] == (self.n_in_channels,)
        assert (self.n_ds2_in_channels is None) or (x_ds2 is not None)
        assert (x_ds2 is None) or (x_ds2.shape[:2] == (x.shape[0]//2, x.shape[1]//2))
        x = x.permute(2,0,1)[None,:,:,:]    # (batch_size=1, n_in_channels, sy, sx)

        # estimate affinity maps from input feature maps
        if x_ds2 is None:
            aff_maps = self.aff_net(x)
        else:
            x_ds2 = x_ds2.permute(2,0,1)[None,:,:,:]    # (batch_size=1, n_ds2_in_channels, sy//2, sx//2)
            aff_maps = self.aff_net(x, x_ds2)              # (batch_size=1, 2 or 4, sy, sx) UNNORMALIZED VALUES
        aff_maps = torch.sigmoid(aff_maps).reshape(aff_maps.shape[1:]) # (2 or 4, sy, sx) VALUES IN [0,1]

        if self.two_channel_aff is True:
            assert aff_maps.shape[0] == 2
            aff_maps = torch.cat([aff_maps, 1.-aff_maps], dim=0) # (4, sy, sx) VALUES IN [0,1]

        # refine label maps with affinity maps
        refinement_iter_func = self.refinement_iter_training if self.iter_mode_is_training is True else self.refinement_iter_eval
        y_pred = refinement_iter_func(y_in, aff_maps)   # (sy, sx, n_cat)

        # make predictions harder (closer to 0 or 1 probabilities - too soft probabilities may cause the loss functions to work poorly)
        if self.p_pred_power_k is not None:
            sy, sx, n_categories = y_pred.shape
            y_pred_2d = y_pred.reshape(sy*sx, n_categories)
            y_pred_2d = Util.prob_vector_power_torch(y_pred_2d, self.p_pred_power_k)
            y_pred = y_pred_2d.reshape(sy, sx, n_categories)

        return y_pred, aff_maps


    def loss(self, pred, label):
        '''
        mean IoU loss ()
        Parameters:
            pred, label: T(sy, sx, n_categories) of fl32 PROBABILITIES
        Returns:
            loss: float (T scalar)
        '''
        sy, sx, n_categories = pred.shape
        assert pred.shape == label.shape
        assert pred.ndim == 3
        
        pred_p = pred.reshape(1, sy*sx, n_categories)
        label_p = label.reshape(1, sy*sx, n_categories)
        return self.loss_expr(pred_p, label_p)
    #

def ce_loss_from_probabilities(pred, label):
    # Cross-entropy loss in PyTorch expecting PROBABILITIES (not LOGITS like torch.nn.CrossEntropyLoss)
    # pred: T(batch_size, n_samples, n_cats) of fl32; PROBABILITIES
    # label: T(batch_size, n_samples, n_cats) of fl32; PROBABILITIES
    # -> T scalar
    assert pred.shape == label.shape
    assert pred.ndim == 3
    EPS = 1e-12
    pred = torch.clamp(pred, EPS, 1-EPS)
    return -torch.sum(label * torch.log(pred)) / (label.shape[0]*label.shape[1])

class MeanIOULoss_Refinement(nn.Module):

    # Rahman et al. 2016, "Optimizing Intersection-Over-Union in Deep Neural Networks for Image Segmentation"

    def __init__(self, apply_softmax=False, only_fg=False):
        super(MeanIOULoss_Refinement, self).__init__()
        self.apply_softmax = apply_softmax
        self.only_fg = only_fg

    def forward(self, pred, label):
        # pred: T(batch_size, n_pixels, n_cats) of fl32
        # label: T(batch_size, n_pixels, n_cats) of fl32
        # -> T scalar
        assert pred.shape == label.shape
        assert pred.ndim == 3
        batch_size, n_pixels, n_cats = pred.shape

        # apply softmax on predictions if needed
        if self.apply_softmax:
            pred = tnnF.softmax(pred, dim=-1)

        # if 'only_fg' enabled, do not compute IoU for category #0 (background)
        if self.only_fg is True:
            pred = pred[:,:,1:]
            label = label[:,:,1:]

        # intersection / union
        inter = pred * label
        union = pred + label - inter
        inter = inter.sum(dim=1)            # (batch_size, n_cats)
        union = union.sum(dim=1)            # (batch_size, n_cats)
        iou = inter / (union + 1e-8)
        return 1. - iou.mean(dim=-1).mean(dim=0)

