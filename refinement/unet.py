
# Original code: https://github.com/milesial/Pytorch-UNet/
        
import torch
import torch.nn as nn
import torch.nn.functional as F


class ConvBlock(nn.Module):
    """(convolution => [BN] => ReLU) * n_conv_layers"""

    def __init__(self, in_channels, out_channels, device, mid_channels=None, \
                        batch_norm=True, n_conv_layers=2, dropout_rate=.0):
        super().__init__()
        assert 2 <= n_conv_layers <= 4
        if not mid_channels:
            mid_channels = out_channels
        layers = []
        for conv_layer_idx in range(n_conv_layers):
            layer_in_channels = in_channels if conv_layer_idx == 0 else mid_channels
            layer_out_channels = out_channels if conv_layer_idx == n_conv_layers-1 else mid_channels
            layers.append(nn.Conv2d(layer_in_channels, layer_out_channels, device=device, kernel_size=3, padding=1, bias=True))
            if batch_norm is True:
                layers.append(nn.BatchNorm2d(layer_out_channels, device=device))
            layers.append(nn.ReLU(inplace=True))
        if dropout_rate > .0:
            layers.append(nn.Dropout(p=dropout_rate))
        self.conv_block = nn.Sequential(*layers)

    def forward(self, x):
        return self.conv_block(x)


class Down(nn.Module):
    """Downscaling with maxpool then conv block"""

    def __init__(self, in_channels, out_channels, device, batch_norm=True, n_conv_layers=2, dropout_rate=.0):
        super().__init__()
        self.maxpool_conv = nn.Sequential(
            nn.MaxPool2d(2),
            ConvBlock(in_channels, out_channels, device=device, batch_norm=batch_norm, n_conv_layers=n_conv_layers, \
                            dropout_rate=dropout_rate)
        )

    def forward(self, x):
        return self.maxpool_conv(x)

class Down_Merge_DS2_Input(nn.Module):
    """Downscaling with maxpool then conv block"""

    def __init__(self, in_channels, in_ds2_channels, out_channels, device, batch_norm=True, n_conv_layers=2, dropout_rate=.0):
        super().__init__()
        self.maxpool = nn.MaxPool2d(2)
        self.conv = ConvBlock(in_channels+in_ds2_channels, out_channels, device=device, batch_norm=batch_norm, \
                                n_conv_layers=n_conv_layers, dropout_rate=dropout_rate)

    def forward(self, x, x_ds2):
        x = self.maxpool(x)
        return self.conv(torch.cat([x, x_ds2], dim=1))


class Up(nn.Module):
    """Upscaling then conv block"""

    def __init__(self, in_channels, out_channels, device, batch_norm=True, n_conv_layers=2, dropout_rate=.0):
        super().__init__()

        self.up = nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True)
        self.conv = ConvBlock(in_channels, out_channels, device=device, mid_channels=in_channels // 2, \
                                batch_norm=batch_norm, n_conv_layers=n_conv_layers, dropout_rate=dropout_rate)

    def forward(self, x1, x2):
        x1 = self.up(x1)
        # input is CHW
        diffY = x2.size()[2] - x1.size()[2]
        diffX = x2.size()[3] - x1.size()[3]

        x1 = F.pad(x1, [diffX // 2, diffX - diffX // 2,
                        diffY // 2, diffY - diffY // 2])
        # if you have padding issues, see
        # https://github.com/HaiyongJiang/U-Net-Pytorch-Unstructured-Buggy/commit/0e854509c2cea854e247a9c615f175f76fbb2e3a
        # https://github.com/xiaopeng-liao/Pytorch-UNet/commit/8ebac70e633bac59fc22bb5195e513d5832fb3bd
        x = torch.cat([x2, x1], dim=1)
        return self.conv(x)


class OutConv(nn.Module):
    def __init__(self, in_channels, out_channels, device):
        super(OutConv, self).__init__()
        self.conv = nn.Conv2d(in_channels, out_channels, device=device, kernel_size=1)

    def forward(self, x):
        return self.conv(x)


class UNet_DS2_Input(nn.Module):
    def __init__(self, n_in_channels, n_out_channels, device, n_ds2_in_channels=None, n_channels_mult=64, n_scales=4, \
                    batch_norm=True, n_conv_layers_per_block=2, dropout_rate=.0):
        super(UNet_DS2_Input, self).__init__()
        self.n_in_channels = n_in_channels
        self.n_out_channels = n_out_channels
        self.device = device
        self.n_ds2_in_channels = n_ds2_in_channels
        assert 1 <= n_scales <= 6
        self.n_scales = n_scales

        self.inc = ConvBlock(n_in_channels, n_channels_mult, device=device, batch_norm=batch_norm, \
                                n_conv_layers=n_conv_layers_per_block, dropout_rate=dropout_rate)
        self.downs = []
        self.ups = []
        max_scale_idx = min(4, self.n_scales-1)
        for scale_idx in range(self.n_scales):
            # downscaling (encoder) block
            n_block_in_channels = n_channels_mult * (2**min(scale_idx, max_scale_idx))
            n_block_out_channels = n_channels_mult * (2**min(scale_idx+1, max_scale_idx))
            if (scale_idx == 0) and (n_ds2_in_channels is not None):
                self.downs.append(Down_Merge_DS2_Input(n_block_in_channels, n_ds2_in_channels, n_block_out_channels, device=device, \
                                                            batch_norm=batch_norm, n_conv_layers=n_conv_layers_per_block, \
                                                            dropout_rate=dropout_rate))
            else:
                self.downs.append(Down(n_block_in_channels, n_block_out_channels, device=device, batch_norm=batch_norm, \
                                        n_conv_layers=n_conv_layers_per_block, dropout_rate=dropout_rate))
            
            # upscaling (decoder) block - dropout disabled
            n_block_in_channels = n_channels_mult * (2**(min(scale_idx, max_scale_idx)+1))
            n_block_out_channels = n_channels_mult * (2**min(scale_idx, max_scale_idx))
            if 1 <= scale_idx <= 4:
                n_block_out_channels //= 2
            self.ups.append(Up(n_block_in_channels, n_block_out_channels, device=device, \
                        batch_norm=batch_norm, n_conv_layers=n_conv_layers_per_block, dropout_rate=0.0))  # reversed order: last decoder layer first

        self.downs = nn.Sequential(*self.downs)
        self.ups = nn.Sequential(*self.ups)
        self.outc = OutConv(n_channels_mult, n_out_channels, device=device)

    def forward(self, x, x_ds2=None):
        # (B, C_in, H, W), OPTIONAL (B, C_in_ds2, H//2, W//2) -> (B, C_out, H, W)
        assert len(self.downs) == len(self.ups)
        assert (self.n_ds2_in_channels is None) or (x_ds2 is not None)
        assert (self.n_ds2_in_channels is not None) or (x_ds2 is None)

        block_outputs = [self.inc(x)]
        for down_block_idx in range(len(self.downs)):
            down_block = self.downs[down_block_idx]
            if (down_block_idx == 0) and (self.n_ds2_in_channels is not None):
                # first down block has two inputs: x, x_ds2
                block_outputs.append(down_block(block_outputs[-1], x_ds2))
            else:
                block_outputs.append(down_block(block_outputs[-1]))

        up_block_inp1 = block_outputs[-1]
        for up_block_idx in range(len(self.ups)-1, -1, -1):   # reversed iteration
            up_block = self.ups[up_block_idx]
            up_block_inp2 = block_outputs[up_block_idx]
            up_block_inp1 = up_block(up_block_inp1, up_block_inp2)
        logits = self.outc(up_block_inp1)
        return logits
    #