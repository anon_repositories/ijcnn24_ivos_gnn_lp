
# 
#   Training/eval data generator class for CNNs.
#       Non-blocking data generator for refinement training.
#       Suitable for cases when all VideoData objects needed for training would not fit into memory at once.
#   In the background, the class iterates thorugh all videos from time to time and generates training data chunks 
#       which contain samples from each video uniformly. 
#       The actual iterator interface implemented by this class iterates through samples from these data chunks and returns
#       them in batches. When most of the available samples have been used up, the chunk generation process is repeated.
#

import queue
import multiprocessing
import numpy as np
import cv2
import torch
import torch.nn.functional as tnnF
import config as Config
from cache_manager import CacheManager

# Async methods called in a new process

def _create_n_samples_from_video(n_samples, seg, bgr_ims, of_fw, of_bw, annot_im, fmap_ds2, gnn_preds, step_idxs=None):
    '''
    Parameters:
        n_samples: int
        seg: Segmentation
        bgr_ims: ndarray(n_fr, sy, sx, 3) of ui8
        of_fw: ndarray(n_fr, sy, sx, 2:[dy, dx]) of fl16
        of_bw: ndarray(n_fr, sy, sx, 2:[dy, dx]) of fl16
        annot_im: ndarray(n_fr, sy, sx) of ui8
        fmap_ds2: ndarray(n_fr, sy//2, sx//2, n_feat) of fl16
        gnn_preds: list; see DAVIS17.get_refinement_training_data() for details on format
        (OPTIONAL) step_idxs: ndarray(n_possible_step_idxs,) of int; step indices to select from when generating samples
    Returns:
        ret_fr_idxs: ndarray(n_samples,) of i32
        ret_session_idxs: ndarray(n_samples,) of i32
        ret_step_idxs: ndarray(n_samples,) of i32
        ret_bgr_ims: ndarray(n_samples, sy, sx, 3) of ui8
        ret_of_fw: ndarray(n_samples, sy, sx, 2:[dy, dx]) of fl16
        ret_of_bw: ndarray(n_samples, sy, sx, 2:[dy, dx]) of fl16
        ret_annot_im: ndarray(n_samples, sy, sx) of ui8
        ret_fmap_ds2: ndarray(n_samples, sy//2, sx//2, n_feat) of fl16
        ret_seg_ims: ndarray(n_samples, sy, sx) of ui16
        ret_gnn_preds: ndarray(n_samples) of ndarray(n_segs_in_frame, p_dim) of fl32; first array is of object type,
                                                                 as n_segs_in_frame and p_dim can vary in different videos
    '''
    assert n_samples >= 1
    n_frames_in_vid = bgr_ims.shape[0]

    # generate a random frame index for each sample, 
    n_samples_collected = 0
    ret_fr_idxs = []
    if n_samples >= n_frames_in_vid:
        n_rep = n_samples // n_frames_in_vid
        fr_idxs = np.arange(n_rep*n_frames_in_vid) % n_frames_in_vid
        np.random.shuffle(fr_idxs)
        ret_fr_idxs.append(fr_idxs)
        n_samples_collected += fr_idxs.shape[0]
    fr_idxs = np.random.choice(n_frames_in_vid, size=(n_samples-n_samples_collected), replace=False)
    ret_fr_idxs.append(fr_idxs)
    ret_fr_idxs = np.concatenate(ret_fr_idxs, axis=0).astype(np.int32)  # (n_samples,) of i32

    # generate gnn_preds session and step idxs
    n_sessions = len(gnn_preds)
    assert len(gnn_preds) >= 1
    n_steps = len(gnn_preds[0])
    assert (step_idxs is None) or (len(step_idxs) >= 1)
    step_idxs = np.arange(n_steps) if step_idxs is None else step_idxs
    ret_session_idxs = np.random.randint(n_sessions, size=(n_samples,), dtype=np.int32)
    ret_step_idxs = np.random.choice(step_idxs, size=(n_samples,)).astype(np.int32)

    # collect and return data
    ret_bgr_ims = bgr_ims[ret_fr_idxs,:,:,:]
    ret_of_fw = of_fw[ret_fr_idxs,:,:,:]
    ret_of_bw = of_bw[ret_fr_idxs,:,:,:]
    ret_annot_im = annot_im[ret_fr_idxs,:,:]
    ret_fmap_ds2 = fmap_ds2[ret_fr_idxs,:,:,:]

    # create gnn preds data and corresponding frame segmentations
    ret_seg_ims = np.empty(ret_bgr_ims.shape[:3], dtype=np.uint16)
    ret_gnn_preds = np.empty((n_samples,), dtype=np.object)   # Numpy initializes values as refs to None objects
    for sample_idx, fr_idx in enumerate(ret_fr_idxs):
        ret_seg_ims[sample_idx,:,:] = seg.get_seg_im_frame(fr_idx, framewise_seg_ids=True)
        fr_seg_offset, fr_seg_end_offset = seg.get_fr_seg_id_offset(fr_idx), seg.get_fr_seg_id_end_offset(fr_idx)
        ret_gnn_preds[sample_idx] = gnn_preds[ret_session_idxs[sample_idx]][ret_step_idxs[sample_idx]]['davis_state_prev_preds']\
                        [fr_seg_offset:fr_seg_end_offset,:].copy()   # copying array slice

    return ret_fr_idxs, ret_session_idxs, ret_step_idxs, ret_bgr_ims, ret_of_fw, ret_of_bw, ret_annot_im, ret_fmap_ds2, ret_seg_ims, ret_gnn_preds

def _create_new_chunk(vidnames, step_idxs, n_samples_per_chunk, n_segments_per_chunk):
    '''
    Creates a new training data chunk.
        The returned samples are uniformly distributed across all videos and are shuffled.
    Parameters:
        vidnames: str
        step_idxs = list of ints
        n_samples_per_chunk: int
        n_segments_per_chunk: int
    Returns:
        new_chunk_dict: dict{str: list of ndarray}
    '''
    assert n_segments_per_chunk >= 1
    assert n_samples_per_chunk // n_segments_per_chunk >= 1
    cache_manager = CacheManager(n_vids_in_memory_limit=3, loading_protocol='refine_train', release_mode='eager')

    new_chunk_dict = None
    n_samples_per_video = n_samples_per_chunk // len(vidnames)
    n_samples_total = n_samples_per_video * len(vidnames)
    assert n_samples_per_video >= 1

    sample_offset = 0
    for vid_idx, vidname in enumerate(vidnames):
        curr_video_data = cache_manager.create_videodata_obj_reference(vidname)

        videodata_bgr_ims = curr_video_data.get_data('bgr_im')     # ndarray(n_fr, sy, sx, 3) of ui8
        videodata_of_fw = curr_video_data.get_data('of_fw_im')     # ndarray(n_fr, sy, sx, 2:[dy, dx]) of fl16
        videodata_of_bw = curr_video_data.get_data('of_bw_im')     # ndarray(n_fr, sy, sx, 2:[dy, dx]) of fl16
        videodata_annot_im = curr_video_data.get_data('annot_im')  # ndarray(n_fr, sy, sx) of ui8
        videodata_fmap_ds2 = curr_video_data.get_data('ssn_fmaps_ds2')  # ndarray(n_fr, sy//2, sx//2, n_feat) of fl16
        videodata_gnn_preds = curr_video_data.get_data('gnn_preds_refinement_training_data')  #  see DAVIS17.get_refinement_training_data() for details on format

        ret_fr_idxs, ret_session_idxs, ret_step_idxs, ret_bgr_ims, ret_of_fw, ret_of_bw, ret_annot_im, ret_fmap_ds2, ret_seg_ims, ret_gnn_preds =\
            _create_n_samples_from_video(n_samples=n_samples_per_video, seg=curr_video_data.get_seg(), \
                                          bgr_ims=videodata_bgr_ims, of_fw=videodata_of_fw, of_bw=videodata_of_bw, \
                                          annot_im=videodata_annot_im, fmap_ds2=videodata_fmap_ds2, \
                                          gnn_preds=videodata_gnn_preds, step_idxs=step_idxs)
        ret_vid_idxs = np.full_like(ret_fr_idxs, fill_value=vid_idx)

        if new_chunk_dict is None:
            # init (once)

            ar = np.arange(n_samples_total)
            segment_offsets = np.linspace(0, n_samples_total, num=n_segments_per_chunk, endpoint=False, dtype=np.int32)
            segment_idxs = np.digitize(ar, segment_offsets[1:])
            segment_lens = np.diff(segment_offsets, append=[n_samples_total])
            arr_idxs = ar - segment_offsets[segment_idxs]
            chunk_idxs = np.stack([segment_idxs, arr_idxs], axis=1)    # (n_samples, 2)
            np.random.shuffle(chunk_idxs)
            del segment_idxs, arr_idxs      # avoid accidental accessing of unshuffled idxs

            new_chunk_dict = {}
            new_chunk_dict['vid_idxs'] = [np.empty((segment_len,), dtype=np.int32) for segment_len in segment_lens]
            new_chunk_dict['fr_idxs'] = [np.empty((segment_len,), dtype=np.int32) for segment_len in segment_lens]
            new_chunk_dict['session_idxs'] = [np.empty((segment_len,), dtype=np.int32) for segment_len in segment_lens]
            new_chunk_dict['step_idxs'] = [np.empty((segment_len,), dtype=np.int32) for segment_len in segment_lens]
            new_chunk_dict['bgr_ims'] = [np.empty((segment_len,) + ret_bgr_ims.shape[1:], dtype=np.uint8) for segment_len in segment_lens]
            new_chunk_dict['of_fw'] = [np.full((segment_len,) + ret_of_fw.shape[1:], dtype=np.float16, fill_value=np.nan) for segment_len in segment_lens]
            new_chunk_dict['of_bw'] = [np.full((segment_len,) + ret_of_bw.shape[1:], dtype=np.float16, fill_value=np.nan) for segment_len in segment_lens]
            new_chunk_dict['annot_im'] = [np.empty((segment_len,) + ret_annot_im.shape[1:], dtype=np.uint8) for segment_len in segment_lens]
            new_chunk_dict['fmap_ds2'] = [np.full((segment_len,) + ret_fmap_ds2.shape[1:], dtype=np.float16, fill_value=np.nan) for segment_len in segment_lens]
            new_chunk_dict['seg_ims'] = [np.empty((segment_len,) + ret_seg_ims.shape[1:], dtype=np.uint16) for segment_len in segment_lens]
            new_chunk_dict['gnn_preds'] = [np.empty((segment_len,), dtype=np.object) for segment_len in segment_lens]

        # place new samples into chunk following the generated random indices (chunk_idxs)
        for vid_sample_idx in range(ret_fr_idxs.shape[0]):
            sample_idx = sample_offset + vid_sample_idx
            new_chunk_dict['vid_idxs'][chunk_idxs[sample_idx,0]][chunk_idxs[sample_idx,1]] = ret_vid_idxs[vid_sample_idx]
            new_chunk_dict['fr_idxs'][chunk_idxs[sample_idx,0]][chunk_idxs[sample_idx,1]] = ret_fr_idxs[vid_sample_idx]
            new_chunk_dict['session_idxs'][chunk_idxs[sample_idx,0]][chunk_idxs[sample_idx,1]] = ret_session_idxs[vid_sample_idx]
            new_chunk_dict['step_idxs'][chunk_idxs[sample_idx,0]][chunk_idxs[sample_idx,1]] = ret_step_idxs[vid_sample_idx]
            new_chunk_dict['bgr_ims'][chunk_idxs[sample_idx,0]][chunk_idxs[sample_idx,1]] = ret_bgr_ims[vid_sample_idx,:,:,:]
            new_chunk_dict['of_fw'][chunk_idxs[sample_idx,0]][chunk_idxs[sample_idx,1]] = ret_of_fw[vid_sample_idx,:,:,:]
            new_chunk_dict['of_bw'][chunk_idxs[sample_idx,0]][chunk_idxs[sample_idx,1]] = ret_of_bw[vid_sample_idx,:,:,:]
            new_chunk_dict['annot_im'][chunk_idxs[sample_idx,0]][chunk_idxs[sample_idx,1]] = ret_annot_im[vid_sample_idx,:,:]
            new_chunk_dict['fmap_ds2'][chunk_idxs[sample_idx,0]][chunk_idxs[sample_idx,1]] = ret_fmap_ds2[vid_sample_idx,:,:,:]
            new_chunk_dict['seg_ims'][chunk_idxs[sample_idx,0]][chunk_idxs[sample_idx,1]] = ret_seg_ims[vid_sample_idx,:,:]
            new_chunk_dict['gnn_preds'][chunk_idxs[sample_idx,0]][chunk_idxs[sample_idx,1]] = ret_gnn_preds[vid_sample_idx]

        sample_offset += ret_fr_idxs.shape[0]
        cache_manager.release_videodata_obj_reference(vidname)

    return new_chunk_dict

def _start_creating_new_chunks_async(chunks_queue, vidnames, step_idxs, n_samples_per_chunk, n_segments_per_chunk):
    '''
    BLOCKING.
    Creates new training data chunks infinitely and puts them into the given queue one by one.
        The procedure is blocking while the queue is full.
    Parameters:
        (MODIFIED) chunks_queue: queue.Queue; output chunk dict will be put into it
        vidnames: str
        step_idxs = list of ints
        n_samples_per_chunk: int
        n_segments_per_chunk: int
    '''
    while True:
        new_chunk_dict = _create_new_chunk(vidnames, step_idxs, n_samples_per_chunk, n_segments_per_chunk)
        while True:
            try:
                chunks_queue.put(new_chunk_dict, block=True, timeout=2)
                del new_chunk_dict
                break
            except queue.Full:
                pass
    #


# RefinementDataGenerator_Async class

class RefinementDataGenerator_Async:

    '''
    Iterator. Spawns a worker thread.
    Member fields:
        n_samples_per_chunk: int
        queue_maxsize: int
        no_parallelism: bool; if True, data loading is executed in the process of self, no new processes are spawned.

        vidnames: list of str
        target_device: str; target device (e.g., 'cuda')
        (OPTIONAL) augmenter: Augmenter instance 

        process_manager: multiprocessing.Manager
        chunks_queue: queue.Queue
        loader_process: multiprocessing.Process

        current_chunk_dict: None OR dict
        current_sample_idx_in_chunk_segment: int
        step_idxs: list of int; the step idxs used for training from the gnn prediciton data
    '''

    def __init__(self, vidnames, target_device, n_samples_per_chunk, augmenter=None, queue_maxsize=1, no_parallelism=False):
        assert int(n_samples_per_chunk) >= 100
        self.n_samples_per_chunk = int(n_samples_per_chunk)
        assert int(queue_maxsize) >= 1
        self.queue_maxsize = int(queue_maxsize)
        self.vidnames = list(sorted(list(vidnames)))
        self.target_device = target_device
        self.augmenter = augmenter

        self.current_chunk_dict = None
        self.current_sample_idx_in_chunk_segment = 0

        self.step_idxs = None if Config.REFINEMENT_STEP_IDXS_TRAINING is None else Config.REFINEMENT_STEP_IDXS_TRAINING.copy() 
        self.loader_process = None  # this way the destructor won't raise an error if the following statements fail
        self.no_parallelism = no_parallelism

        # Start worker thread for asynchronous loading
        if self.no_parallelism is False:
            self.process_manager = multiprocessing.Manager()
            self.chunks_queue = self.process_manager.Queue(maxsize=self.queue_maxsize)
            step_idxs = None if self.step_idxs is None else self.step_idxs.copy()
            self.loader_process = multiprocessing.Process(target=_start_creating_new_chunks_async, \
                                    kwargs={'chunks_queue': self.chunks_queue, 'vidnames': self.vidnames.copy(), \
                                            'step_idxs': step_idxs, 'n_samples_per_chunk': self.n_samples_per_chunk, \
                                            'n_segments_per_chunk': 5})
            self.loader_process.start()

    def get_n_xfeatures(self):
        return 7

    def __iter__(self):
        return self

    def __del__(self):
        # Destructor to explicitly end worker process
        if self.loader_process is not None:
            self.loader_process.terminate()

    def __next__(self):
        '''
        Blocking.
        Returns a single training sample as a batch (GPU tensors). Also returns image data for easy visualization.
        Returns:
            x_gpu: T(sy, sx, x_channels) of fl32; channels: [lab:3, of_fw:2, of_bw:2]
            x_ds2_gpu: T(sy//2, sx//2, x_ds2_channels) of fl32; channels: [fmap:n_features]
            y_in_gpu: T(sy, sx, n_categories) of fl32; the label map to refine  (one-hot probabilities)
            y_true_gpu: T(sy, sx, n_categories) of fl32; the true label map (one-hot probabilities)
            im_bgr_npy: ndarray(sy, sx, 3:[BGR]) of uint8
        '''
        sample_dict = self._get_next_sample_from_current_chunk()   # arrays for the single sample with the original key names in the dict

        gnn_preds = sample_dict['gnn_preds']
        n_categories = gnn_preds.shape[-1]

        # creat input label
        seg_im_gpu = torch.from_numpy(sample_dict['seg_ims'].astype(np.int64)).to(self.target_device).to(torch.int64)  # torch does not support uint16
        gnn_preds_gpu = torch.from_numpy(gnn_preds).to(self.target_device)
        y_in_gpu = gnn_preds_gpu[seg_im_gpu,:]    # T(sy, sx, n_categories) of fl32

        # create target label
        annot_im = sample_dict['annot_im']
        annot_im_gpu = torch.from_numpy(annot_im).to(self.target_device).to(torch.int64)
        y_true_gpu = tnnF.one_hot(annot_im_gpu, num_classes=n_categories).to(torch.float32)  # T(sy, sx, n_categories) of fl32

        # collect full resolution input features
        im_bgr_npy = sample_dict['bgr_ims']     # ndarray(sy, sx, 3) of ui8
        sy, sx = im_bgr_npy.shape[:2]
        x_lab = cv2.cvtColor(im_bgr_npy, cv2.COLOR_BGR2LAB).astype(np.float32)/255.     # ndarray(sy, sx, 3) of fl32
        x_lab_gpu = torch.from_numpy(x_lab).to(self.target_device)   # T(sy, sx, 3)
        x_of_fw = sample_dict['of_fw']                                  # ndarray(sy, sx, 2:[dy, dx]) of fl16
        x_of_bw = sample_dict['of_bw']                                  # ndarray(sy, sx, 2:[dy, dx]) of fl16
        x_of_fw_gpu = torch.from_numpy(x_of_fw).to(self.target_device).to(torch.float32)   # T(sy, sx, 2)
        x_of_bw_gpu = torch.from_numpy(x_of_bw).to(self.target_device).to(torch.float32)   # T(sy, sx, 2)
        x_gpu = torch.cat([x_lab_gpu, x_of_fw_gpu, x_of_bw_gpu], dim=2)                 # T(sy, sx, 7)

        # collect ds2 input features
        fmap_ds2 = sample_dict['fmap_ds2']     # ndarray(sy//2, sx//2, n_features) of fl16
        x_ds2_gpu = torch.from_numpy(fmap_ds2).to(self.target_device).to(torch.float32)   # T(sy//2, sx//2, n_features)

        # augment if needed
        if self.augmenter is not None:
            x_gpu, x_ds2_gpu, y_in_gpu, y_true_gpu, im_bgr_npy = self.augmenter.augment(x_gpu, x_ds2_gpu, y_in_gpu, y_true_gpu, im_bgr_npy)

        return x_gpu, x_ds2_gpu, y_in_gpu, y_true_gpu, im_bgr_npy

    def create_pred_sample(self, video_data, fr_idx, y_in_seg):
        '''
        Parameters:
            video_data: VideoData
            fr_idx: int
            y_in_seg: ndarray(n_seg, n_cat) of fl32
        Returns:
            x_gpu: T(sy, sx, x_channels) of fl32; channels: [lab:3, of_fw:2, of_bw:2]
            x_ds2_gpu: T(sy//2, sx//2, x_ds2_channels) of fl32; channels: [fmap:n_features]
            y_in_gpu: T(sy, sx, n_categories) of fl32; the label map to refine  (one-hot probabilities)
        '''
        # create input labels (from gnn prediction)
        fr_seg_offset = video_data.get_seg().get_fr_seg_id_offset(fr_idx)
        fr_seg_end_offset = video_data.get_seg().get_fr_seg_id_end_offset(fr_idx)
        seg_im_fwise_npy = video_data.get_seg().get_seg_im_frame(fr_idx, framewise_seg_ids=True)
        seg_im_gpu = torch.from_numpy(seg_im_fwise_npy.astype(np.int64)).to(self.target_device).to(torch.int64)  # torch does not support uint16
        gnn_preds_gpu = torch.from_numpy(y_in_seg).to(self.target_device)
        gnn_preds_gpu_fr = gnn_preds_gpu[fr_seg_offset:fr_seg_end_offset,:]
        y_in_gpu = gnn_preds_gpu_fr[seg_im_gpu,:]    # T(sy, sx, n_categories) of fl32

        # collect full resolution input features
        im_bgr_npy = video_data.get_data('bgr_im')[fr_idx,:,:,:]   # ndarray(sy, sx, 3) of ui8
        sy, sx = im_bgr_npy.shape[:2]
        x_lab = cv2.cvtColor(im_bgr_npy, cv2.COLOR_BGR2LAB).astype(np.float32)/255.     # ndarray(sy, sx, 3) of fl32
        x_lab_gpu = torch.from_numpy(x_lab).to(self.target_device)   # T(sy, sx, 3)
        x_of_fw = video_data.get_data('of_fw_im')[fr_idx,:,:,:]  # ndarray(sy, sx, 2:[dy, dx]) of fl16
        x_of_bw = video_data.get_data('of_bw_im')[fr_idx,:,:,:]  # ndarray(sy, sx, 2:[dy, dx]) of fl16
        x_of_fw_gpu = torch.from_numpy(x_of_fw).to(self.target_device).to(torch.float32)   # T(sy, sx, 2)
        x_of_bw_gpu = torch.from_numpy(x_of_bw).to(self.target_device).to(torch.float32)   # T(sy, sx, 2)
        x_gpu = torch.cat([x_lab_gpu, x_of_fw_gpu, x_of_bw_gpu], dim=2)                 # T(sy, sx, 7)

        # collect ds2 input features
        fmap_ds2 = video_data.get_data('ssn_fmaps_ds2')[fr_idx,:,:,:]   # ndarray(sy//2, sx//2, n_features) of fl16
        x_ds2_gpu = torch.from_numpy(fmap_ds2).to(self.target_device).to(torch.float32)   # T(sy//2, sx//2, n_features)
        
        return x_gpu, x_ds2_gpu, y_in_gpu

    def _get_next_sample_from_current_chunk(self):
        '''
        BLOCKING.
        Gets the next sample from the current data chunk. If the current data chunk is exhausted, gets a new one.
        Returns:
            sample_dict: dict containing all array slices corresponding to the next sample
        '''
        # if current chunk is not empty but the current (last) segment is exhausted, drop this segment from the chunk
        if (self.current_chunk_dict is not None) and (len(self.current_chunk_dict['vid_idxs']) >= 1) and \
                            (self.current_sample_idx_in_chunk_segment >= self.current_chunk_dict['vid_idxs'][-1].shape[0]):
            for key in self.current_chunk_dict.keys():
                chunk_segments_list = self.current_chunk_dict[key]
                del chunk_segments_list[-1]
            self.current_sample_idx_in_chunk_segment = 0

        # if no more segments in chunk (or on init), create a new chunk
        if (self.current_chunk_dict is None) or (len(self.current_chunk_dict['vid_idxs']) < 1):
            self.current_chunk_dict = None   # allow release of old chunk segment from memory before starting new chunk creation
            if self.no_parallelism is True:
                self.current_chunk_dict = self._get_new_chunk_no_parallelism()
            else:
                self.current_chunk_dict = self._get_new_chunk_blocking_async()
            self.current_sample_idx_in_chunk_segment = 0

        # (assuming the last segment in the current chunk has at least one sample now) - drawing a sample
        sample_dict = {}
        for key in self.current_chunk_dict.keys():
            sample_arr = self.current_chunk_dict[key][-1][self.current_sample_idx_in_chunk_segment]
            sample_dict[key] = sample_arr

        # increase current sample index within current chunk segment, then return sample
        self.current_sample_idx_in_chunk_segment += 1
        return sample_dict

    def _get_new_chunk_blocking_async(self):
        '''
        BLOCKING.
        Gets a new training data chunk from the queue.
        Returns:
            dict; see _create_new_chunk() for details about the format
        '''
        while True:
            try:
                new_chunk_dict = self.chunks_queue.get(block=True, timeout=2)
                break
            except queue.Empty:
                pass
        return new_chunk_dict

    def _get_new_chunk_no_parallelism(self):
        '''
        Creates a new training data chunk in the current process (no parallelism).
        Returns:
            dict; see _create_new_chunk() for details about the format
        '''
        step_idxs = None if self.step_idxs is None else self.step_idxs.copy()
        return _create_new_chunk(vidnames=self.vidnames.copy(), step_idxs=self.step_idxs, \
                                                n_samples_per_chunk=self.n_samples_per_chunk, n_segments_per_chunk=5)

