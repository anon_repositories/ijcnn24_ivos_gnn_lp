
# Runner script to train a GNN model.

import config as Config
import sys
sys.path.append(Config.PATH_DAVIS_INTERACTIVE_TRAINING)

import numpy as np
import torch
import random

from cache_manager import CacheManager
from seed_propagation.basic_optflow_seed_propagation import BasicOptflowSeedPropagation
from label_estimation.gnn_label_estimation import GNNLabelEstimation
from label_estimation.graph_datagen_davis_multi import GraphTrainingDatasetDAVIS
from label_estimation.nearestneighbor_label_model import NearestNeighborLabelModel

if __name__ == '__main__':

    #SET_NAME = 'debug (3+3)'
    SET_NAME = 'full (60+30)'

    print("CONFIG --->")
    print({var: Config.__dict__[var] for var in dir(Config) if not var.startswith("__")})
    print("<--- CONFIG")

    N_VIDS_IN_MEMORY_LIMIT = 64
    cache_manager = CacheManager(n_vids_in_memory_limit=N_VIDS_IN_MEMORY_LIMIT, loading_protocol='cache_only')
    DatasetClass = CacheManager.get_dataset_class()
    train_vidnames = DatasetClass.get_video_set_vidnames(SET_NAME, 'train')

    print("Loading all videos & data in order to find maximum n_labels value.")
    cache_manager.ensure_cache_exists(train_vidnames)
    n_labels_max = 0
    for vidname in train_vidnames:
        n_labels_max = max(n_labels_max, cache_manager.create_videodata_obj_reference(vidname).get_data('n_labels'))
        cache_manager.release_videodata_obj_reference(vidname)
    cache_manager.set_loading_protocol('gnn_traineval')

    # INIT LabelModel & SeedProp
    seedprop_alg = BasicOptflowSeedPropagation(cache_manager) if Config.USE_SEEDPROP is True else None
    lab_model_init_fn = lambda: NearestNeighborLabelModel(target_device='cuda')
    lab_model = lab_model_init_fn()

    # INIT & TRAIN LabelEstimation
    lab_est = GNNLabelEstimation(label_model=lab_model, multiclass_n_cats=n_labels_max, seedprop_alg=seedprop_alg)
    tr_iter = GraphTrainingDatasetDAVIS(cache_manager=cache_manager, lab_model_init_fn=lab_model_init_fn, \
                    n_parallel_sessions=Config.GNN_N_PARALLEL_DAVIS_TR_SESSIONS, davis_subset='train', seedprop_alg=seedprop_alg)

    lab_est.train_gnn(tr_iter)

